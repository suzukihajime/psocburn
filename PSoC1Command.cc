// PSoC1Command.cc
//    2012.2 aasoukai128


#include "PSoC1Command.h"


PSoC1Command::PSoC1Command(PICProgrammer &prog)
  : m_prog(prog)
{
}

PSoC1Command::~PSoC1Command()
{
}

void PSoC1Command::writeCommand(unsigned char addr, unsigned char cmd)
{
	m_prog.commandissp(PICProgrammer::issp_cmd_write, addr, cmd);
	return;
}

void PSoC1Command::writeNullCommand(void)
{
	m_prog.commandissp(PICProgrammer::issp_cmd_null, 0, 0);
	return;
}

void PSoC1Command::readData(unsigned char addr, unsigned char *rbuf)
{
	*rbuf = m_prog.commandissp(PICProgrammer::issp_data_read, addr, 0);
	return;
}

void PSoC1Command::writeData(unsigned char addr, unsigned char data)
{
	m_prog.commandissp(PICProgrammer::issp_data_write, addr, data);
	return;
}

int PSoC1Command::wordWriteCycle(int cmd,int begin,int end,int wai,const unsigned short *wbuf,int count)
{
	return 0;
}

int PSoC1Command::wordReadCycle(int cmd,unsigned short *rbuf,int count)
{
	return 0;
}

void PSoC1Command::enterDebugMode(void)
{
	/*
	* device acquire (5V only)
	*/
	m_prog.powerOn();
	m_prog.reset(PICProgrammer::NormalRelease, PICProgrammer::PositiveReset);
	m_prog.acquireissp();			// acquireisspは最初の5コマンドを送る
	// Initialize vector 1
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf7, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf6, 0x00);
	m_prog.commandissp(PICProgrammer::issp_data_write, 0xf8, 0x3a);
	m_prog.commandissp(PICProgrammer::issp_data_write, 0xf9, 0x03);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf5, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf4, 0x03);
	m_prog.commandissp(PICProgrammer::issp_data_write, 0xfb, 0x80);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf9, 0x30);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xfa, 0x40);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf0, 0x09);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf8, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xff, 0x12);
	/*
	* Initialize vector 2
	*/
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf7, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf6, 0x00);
	m_prog.commandissp(PICProgrammer::issp_data_write, 0xf8, 0x3a);
	m_prog.commandissp(PICProgrammer::issp_data_write, 0xf9, 0x03);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf5, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf4, 0x03);
	m_prog.commandissp(PICProgrammer::issp_data_write, 0xfb, 0x80);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf9, 0x30);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xfa, 0x40);
	m_prog.commandissp(PICProgrammer::issp_data_write, 0xfa, 0x01);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf0, 0x06);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf8, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xff, 0x12);
	/*
	* Initialize vector 3
	*/
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf7, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf4, 0x03);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf5, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf6, 0x08);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf8, 0x51);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf9, 0xfc);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xfa, 0x30);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xff, 0x12);
	m_prog.commandissp(PICProgrammer::issp_cmd_null, 0x00, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf7, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf4, 0x03);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf5, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf6, 0x08);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf8, 0x60);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf9, 0xea);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xfa, 0x30);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf7, 0x10);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xff, 0x12);
	m_prog.commandissp(PICProgrammer::issp_cmd_null, 0x00, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf7, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf4, 0x03);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf5, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf6, 0x08);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf8, 0x51);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf9, 0xfd);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xfa, 0x30);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xff, 0x12);
	m_prog.commandissp(PICProgrammer::issp_cmd_null, 0x00, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf7, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf4, 0x03);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf5, 0x00);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf6, 0x08);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf8, 0x60);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf9, 0xe8);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xfa, 0x30);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xf7, 0x10);
	m_prog.commandissp(PICProgrammer::issp_cmd_write, 0xff, 0x12);
	m_prog.commandissp(PICProgrammer::issp_cmd_null, 0x00, 0x00);
	return;
}

void PSoC1Command::exitDebugMode(void)
{
	m_prog.reset(PICProgrammer::NormalRelease, PICProgrammer::PositiveReset);
	m_prog.powerOn();
	m_prog.delay(10*1000*1000);	// > 10ms
}
/*
void PSoC1Command::configureDevice(void)
{
	
}
*/