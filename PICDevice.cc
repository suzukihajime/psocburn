// device.cc
//    2007.6 Y.Senta
//    2008.4 arms22
//    2008.5 arms22
//    2011.12 aasoukai128 (PSoC support)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PICDevice.h"
//#include "PIC14Memory.h"
//#include "PIC14Strategy.h"
//#include "PIC18Strategy.h"
//#include "PIC24Strategy.h"
#include "PSoC1Strategy.h"
#include "PSoC3Strategy.h"
#include "PSoC5Strategy.h"
#include "properties.h"
#include "utils.h"

PICDevice *PICDevice::m_devices = 0;

PICDevice::PICDevice(const char *c)
{
	m_pNext		   = 0;
	m_pDefault	   = 0;
	m_pName		   = strdup(c);
	m_nIDword	   = 0;
	m_nNvlSize	   = 4;
	m_nBlockSize   = 256;
	m_nArraySize   = 256;
	m_nEccBlockSize = 32;
//	m_nIDAddr	   = 0;
//	m_ePowerUp	   = VddFirst;
//	m_nPMMask	   = 0x3fff;
	m_nPMSize	   = 0;
	m_nConfigSize  = 0;
//	m_nEEMemSize   = 0;
//	m_nConfDspSize = 0;
//	m_nConfAddr1   = 0;
//	m_nConfAddr2   = 0;
//	m_nConfMask1   = 0x3fff;
//	m_nConfMask2   = 0x3fff;
/*	for (int i=0;i<(sizeof(m_aCalibAddrs)/sizeof(m_aCalibAddrs[0]));i++){
		m_aCalibAddrs[i] = 0;
		m_aCalibMasks[i] = 0;
	}*/
//	m_eProgType	   = Internal08;
//	m_eProgTypeC   = Internal08;
//	m_nProgWait	   = 40;
//	m_nProgWaitD   = 40;
//	m_nProgWaitC   = 40;
//	m_nProgLatch   = 1;
//	m_eEraseType   = BulkErase_F84;
//	m_nEraseWait   = 40;
	
//	m_bPIC18		= (strncasecmp(m_pName,"pic18",5) == 0);
//	m_nWriteBufSize	= 0;
//	m_nEraseCommand	= 0x80;
//	m_eEEWriteType	= PollWRbit;
//	m_nPanelNum		= 0;
//	m_nPanelSize	= 0;
//	m_nProgWaitID	= 50;
/*	for (int i=0;i<(sizeof(m_aConfigMask)/sizeof(m_aConfigMask[0]));i++){
		m_aConfigMask[i]  = 0;
		m_aConfigValue[i] = 0;
	}*/
	
//	m_bdsPIC		= (strncasecmp(m_pName,"dspic",5) == 0);
//	m_bPIC24		= (strncasecmp(m_pName,"pic24",5) == 0);
/*	for (int i=0;i<(sizeof(m_WriteLatches)/sizeof(m_WriteLatches[0]));i++)
		m_WriteLatches[i] = 0;
	for (int i=0;i<(sizeof(m_NVMCONValues)/sizeof(m_NVMCONValues[0]));i++)
		m_NVMCONValues[i] = 0;*/
//	m_nExecCodeSize	= 0;
	
	if (strncasecmp(m_pName,"CY8C2",5) == 0){
		m_productFamily = PSoC1Family;
	}else if (strncasecmp(m_pName, "CY8C3",5) == 0){
		m_productFamily = PSoC3Family;
	}else if (strncasecmp(m_pName, "CY8C5", 5) == 0) {
		m_productFamily = PSoC5Family;
	}/*else if (strncasecmp(m_pName,"pic18",5) == 0){
		m_productFamily = PIC18Family;
	}else if (strncasecmp(m_pName,"dspic",5) == 0){
		m_productFamily = dsPICFamily;
	}else if (strncasecmp(m_pName,"pic24",5) == 0){
		m_productFamily = PIC24Family;
	}else{
		m_productFamily = PIC10_PIC12_PIC16Family;
	}*/
}

PICDevice::~PICDevice()
{
	if (m_pName)
		free(m_pName);
}

bool PICDevice::load(FILE *fp)
{
	char buf[1024];
	
	while (fgets(buf, sizeof(buf), fp)){
		char *p = buf;
		
		// skip space
		while(*p==' ' || *p=='\t')
			p++;
		
		// skip empty line
		while(*p=='\n' || *p=='\r')
			continue;
		
		// skip comment line
		if (*p==COMMENTCHAR)
			continue;
		
		dbprintf(20, "Loading... %s", p);
		
		if (cfgstrcmp(p, DEFENDSTR)){
			break;
		}
		
		char *v;
		if ((v = cfgstrcmp(p, DEFAULTSTR))){
			PICDevice *dev = searchDeviceFromName(v);
			if (dev){
				m_pDefault	   = dev;
				m_nIDword 	   = dev->m_nIDword;
//				m_nIDAddr 	   = dev->m_nIDAddr;
//				m_ePowerUp 	   = dev->m_ePowerUp;
//				m_nPMMask 	   = dev->m_nPMMask;
				m_nPMSize 	   = dev->m_nPMSize;
				m_nConfigSize  = dev->m_nConfigSize;
				m_nBlockSize   = dev->m_nBlockSize;
				m_nEccBlockSize = dev->m_nEccBlockSize;
				m_nSiliconRev  = dev->m_nSiliconRev;
				m_nNvlSize	   = dev->m_nNvlSize;
//				m_nConfDspSize = dev->m_nConfDspSize;
//				m_nConfAddr1   = dev->m_nConfAddr1;
//				m_nConfAddr2   = dev->m_nConfAddr2;
//				m_nConfMask1   = dev->m_nConfMask1;
//				m_nConfMask2   = dev->m_nConfMask2;
/*				for (int i=0;i<(sizeof(m_aCalibAddrs)/sizeof(m_aCalibAddrs[0]));i++){
					m_aCalibAddrs[i] = dev->m_aCalibAddrs[i];
					m_aCalibMasks[i] = dev->m_aCalibMasks[i];
				}*/
//				m_eProgType    = dev->m_eProgType;
//				m_eProgTypeC   = dev->m_eProgTypeC;
//				m_nProgWait    = dev->m_nProgWait;
//				m_nProgWaitD   = dev->m_nProgWaitD;
//				m_nProgWaitC   = dev->m_nProgWaitC;
//				m_nProgLatch   = dev->m_nProgLatch;
//				m_eEraseType   = dev->m_eEraseType;
//				m_nEraseWait   = dev->m_nEraseWait;
//				m_nWriteBufSize	= dev->m_nWriteBufSize;
//				m_nEraseCommand	= dev->m_nEraseCommand;
//				m_eEEWriteType	= dev->m_eEEWriteType;
//				m_nPanelNum		= dev->m_nPanelNum;
//				m_nPanelSize	= dev->m_nPanelSize;
//				m_nProgWaitID	= dev->m_nProgWaitID;
/*				for(int i=0;i<(sizeof(m_aConfigMask)/sizeof(m_aConfigMask[0]));i++){
					m_aConfigMask[i]  = dev->m_aConfigMask[i];
					m_aConfigValue[i] = dev->m_aConfigValue[i];
				}*/
/*				for(int i=0;i<(sizeof(m_WriteLatches)/sizeof(m_WriteLatches[0]));i++)
					m_WriteLatches[i] = dev->m_WriteLatches[i];
				for(int i=0;i<(sizeof(m_NVMCONValues)/sizeof(m_NVMCONValues[0]));i++)
					m_NVMCONValues[i] = dev->m_NVMCONValues[i];*/
//				m_nExecCodeSize = dev->m_nExecCodeSize;
				m_productFamily = dev->m_productFamily;
			}
			continue;
		}
		
		if ((v = cfgstrcmp(p, IDWORDSTR))){
			if (sscanf(v, "%x", &m_nIDword)	!= 1)
				return false;
			continue;
		}
		
/*		if ((v = cfgstrcmp(p, IDADDRSTR))){
			if (sscanf(v, "%lx", &m_nIDAddr) !=	1)
				return false;
			continue;
		}*/
		/*
		if ((v = cfgstrcmp(p, PWUPTYPESTR))){
			if (!strcmp(v, VDDVPPSTR))
				m_ePowerUp = VddFirst;
			else if (!strcmp(v, VPPVDDSTR))
				m_ePowerUp = VppFirst;
			else
				return false;
			continue;
		}
		*/
/*		if ((v = cfgstrcmp(p, PMMASKSTR))){
			if (sscanf(v, "%x", &m_nPMMask)	!= 1)
				return false;
			continue;
		}*/
		
		if ((v = cfgstrcmp(p, PMSIZESTR))){
			if (sscanf(v, "%d", &m_nPMSize)	!= 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, CFGSIZESTR))){
			if (sscanf(v, "%d", &m_nConfigSize) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, BLKSIZESTR))) {
			if (sscanf(v, "%d", &m_nBlockSize) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, ECCSIZESTR))) {
			if (sscanf(v, "%d", &m_nEccBlockSize) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, SILREVSTR))) {
			if (sscanf(v, "%d", &m_nSiliconRev) != 1)
				return false;
			continue;
		}
		
		if((v = cfgstrcmp(p, NVLSIZESTR))) {
			if (sscanf(v, "%d", &m_nNvlSize) != 1)
				return false;
			continue;
		}
		
/*		if ((v = cfgstrcmp(p, CFGDSPSZSTR))){
			if (sscanf(v, "%d", &m_nConfDspSize) !=	1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, CFGADDR1STR))){
			if (sscanf(v, "%lx", &m_nConfAddr1)	!= 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, CFGADDR2STR))){
			if (sscanf(v, "%lx", &m_nConfAddr2)	!= 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, CFGMSK1STR))){
			if (sscanf(v, "%x", &m_nConfMask1) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, CFGMSK2STR))){
			if (sscanf(v, "%x", &m_nConfMask2) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, CALBADDRSTR))){
			for(int i=0;i<(sizeof(m_aCalibAddrs)/sizeof(m_aCalibAddrs[0]))&&*v;i++){
				if (sscanf(v, "%lx", &m_aCalibAddrs[i]) != 1)
					return false;
				v = chopstr(v+strlen(v)+1);
			}
			continue;
		}
		
		if ((v = cfgstrcmp(p, CALBMASKSTR))){
			for(int i=0;i<(sizeof(m_aCalibMasks)/sizeof(m_aCalibMasks[0]))&&*v;i++){
				if (sscanf(v, "%x", &m_aCalibMasks[i]) != 1)
					return false;
				v = chopstr(v+strlen(v)+1);
			}
		}*/
/*		
		if ((v = cfgstrcmp(p, PRGTYPESTR))){
			if (!strcmp(v, INT08STR))
				m_eProgType	= m_eProgTypeC = Internal08;
			else if (!strcmp(v, INT18STR))
				m_eProgType	= m_eProgTypeC = Internal18;
			else if (!strcmp(v, EXT08STR))
				m_eProgType	= m_eProgTypeC = External08_e;
			else if (!strcmp(v, EXT18STR))
				m_eProgType	= m_eProgTypeC = External18_17;
			else if (!strcmp(v, EXT18ASTR))
				m_eProgType	= m_eProgTypeC = External18_a;
			else
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, PRGTYPECSTR))){
			if (!strcmp(v, INT08STR))
				m_eProgTypeC = Internal08;
			else if (!strcmp(v, INT18STR))
				m_eProgTypeC = Internal18;
			else if (!strcmp(v, EXT08STR))
				m_eProgTypeC = External08_e;
			else if (!strcmp(v, EXT18STR))
				m_eProgTypeC = External18_17;
			else if (!strcmp(v, EXT18ASTR))
				m_eProgTypeC = External18_a;
			else
				return false;
			continue;
		}
		*/
/*		if ((v = cfgstrcmp(p, PRGWAITSTR))){
			if (sscanf(v, "%d", &m_nProgWait) != 1)
				return false;
			m_nProgWaitD = m_nProgWaitC=m_nProgWait;
			continue;
		}
		
		if ((v = cfgstrcmp(p, PRGWAITDSTR))){
			if (sscanf(v, "%d", &m_nProgWaitD) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, PRGWAITCSTR))){
			if (sscanf(v, "%d", &m_nProgWaitC) != 1)
				return false;
			continue;
		}

		if ((v = cfgstrcmp(p, PRGLATCHSTR))){
			if (sscanf(v, "%d", &m_nProgLatch) != 1)
				return false;
			continue;
		}*/
		/*
		if ((v = cfgstrcmp(p, ERSTYPESTR))){
			if (!strcmp(v, BESF509STR))
				m_eEraseType = BulkErase_F509;
			else if (!strcmp(v, BESF519STR))
				m_eEraseType = BulkErase_F519;
			else if (!strcmp(v, BESF675STR))
				m_eEraseType = BulkErase_F675;
			else if (!strcmp(v, BESF683STR))
				m_eEraseType = BulkErase_F683;
			else if (!strcmp(v, BESF84STR))
				m_eEraseType = BulkErase_F84;
			else if (!strcmp(v, BESF648ASTR))
				m_eEraseType = BulkErase_F648A;
			else if (!strcmp(v, CESF88STR))
				m_eEraseType = ChipErase_F88;
			else if (!strcmp(v, CESF77STR))
				m_eEraseType = ChipErase_F77;
			else
				return false;
			continue;
		}
		*/
/*		if ((v = cfgstrcmp(p, ERSWAITSTR))){
			if (sscanf(v, "%d", &m_nEraseWait) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, WRBFSZSTR))){
			if (sscanf(v, "%d", &m_nWriteBufSize) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, CFGMSKSTR))){
			for(int i=0;i<(sizeof(m_aConfigMask)/sizeof(m_aConfigMask[0]))&&*v;i++){
				if (sscanf(v, "%x", m_aConfigMask+i) != 1)
					return false;
				v = chopstr(v+strlen(v)+1);
			}
			continue;
		}
		
		if ((v = cfgstrcmp(p, CFGVALSTR))){
			for(int i=0;i<(sizeof(m_aConfigValue)/sizeof(m_aConfigValue[0]))&&*v;i++){
				if (sscanf(v, "%x", m_aConfigValue+i) != 1)
					return false;
				v = chopstr(v+strlen(v)+1);
				m_nConfDspSize++;
			}
			continue;
		}
		
		if ((v = cfgstrcmp(p, ERSCMDSTR))){
			if (!strcmp(v, ECMD80STR))
				m_nEraseCommand = 0x80;
			else if (!strcmp(v, ECMD3F8FSTR))
				m_nEraseCommand = 0x3F8F;
			else {
				if (sscanf(v, "%x", &m_nEraseCommand) != 1)
					return false;				
			}
			continue;
		}*/
		/*
		if ((v = cfgstrcmp(p, EEWTYPSTR))){
			if (!strcmp(v, PLLWRBITSTR))
				m_eEEWriteType = PollWRbit;
			else if (!strcmp(v, FXWITTMSTR))
				m_eEEWriteType = FixWaitTime;
			else
				return false;
			continue;
		}
		*/
/*		if ((v = cfgstrcmp(p, PNLNUMSTR))){
			if (sscanf(v, "%d", &m_nPanelNum) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, PNLSIZSTR))){
			if (sscanf(v, "%d", &m_nPanelSize) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, PRGWAIPSTR))){
			if (sscanf(v, "%d", &m_nProgWait) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, PRGWAIIDSTR))){
			if (sscanf(v, "%d", &m_nProgWaitID) != 1)
				return false;
			continue;
		}
		
		if ((v = cfgstrcmp(p, WRLATSTR))){
			for(int i=0;i<(sizeof(m_WriteLatches)/sizeof(m_WriteLatches[0]))&&*v;i++){
				if (sscanf(v, "%d", m_WriteLatches+i) != 1)
					return false;
				v = chopstr(v+strlen(v)+1);
			}
			continue;
		}
		
		if ((v = cfgstrcmp(p, NVCOMVALSTR))){
			for(int i=0;i<(sizeof(m_NVMCONValues)/sizeof(m_NVMCONValues[0]));i++){
				if (sscanf(v, "%x", m_NVMCONValues+i) != 1)
					return false;
				v = chopstr(v+strlen(v)+1);
			}
			continue;
		}
		
		if ((v = cfgstrcmp(p, EXECSIZESTR))){
			if (sscanf(v, "%d", &m_nExecCodeSize) != 1)
				return false;
			continue;
		}*/
	}
	
	return true;
}

PICStrategy* PICDevice::createStrategy(PICProgrammer *prog)
{
	PICStrategy *strategy;
	if (m_productFamily & PSoC1Family){
		strategy = new PSoC1Strategy(*this,*prog);	// PSoC1Strategyに書き換える
	}else if (m_productFamily & PSoC3Family){
		strategy = new PSoC3Strategy(*this,*prog);
	}else if (m_productFamily & PSoC5Family){
		strategy = new PSoC5Strategy(*this,*prog);
	}
	/*else if (m_productFamily & (dsPICFamily|PIC24Family)){
		strategy = new PIC24Strategy(*this,*prog);
	}else if (m_productFamily & PIC18Family){
		strategy = new PIC18Strategy(*this,*prog);
	}else{
		strategy = new PIC14Strategy(*this,*prog);
	}*/
	return strategy;
}

bool PICDevice::loadDeviceList(FILE *fin)
{
	char buf[1024];
	
	int nLen = strlen(DEFSTARTSTR);
	while (fgets(buf, sizeof(buf), fin)){
		
		if ((buf[0]==COMMENTCHAR) || (buf[0]=='\r') || (buf[0]=='\n'))
			continue;
		
		if (!strncmp(buf, DEFSTARTSTR, nLen)){
			char *p = chopstr(buf+nLen);
			dbprintf(2, " DeviceLoad [%s]\n", p);
			
			PICDevice *dev = new PICDevice(p);
			if (dev->load(fin)){
				dev->m_pNext =	m_devices;
				m_devices =	dev;
			}else{
				fprintf(stderr, " [ERROR]: [%s] Define error\n", p);
				delete dev;
				while (fgets(buf, sizeof(buf), fin)){
					if (!strncmp(buf, DEFENDSTR, strlen(DEFENDSTR)))
						break;
				}
			}
		} else {
			fprintf(stderr, " [ERROR]: Wrong formatted Define file\n");
			return false;
		}
	}
	return true;
}

PICDevice* PICDevice::searchDeviceFromID(unsigned short devid,unsigned short devid_mask,int family)
{
	if (devid){
		PICDevice *dev = m_devices;
		while (dev){
			if((dev->GetDeviceID() == (devid & devid_mask)) &&
			   (dev->productFamily() & family)){
				return dev;
			}
			dev = dev->m_pNext;
		}
	}
	return 0;
}

PICDevice* PICDevice::deviceAutoDetect(PICProgrammer *prog)
{
//	PIC14Command cmd14(*prog);
	PICDevice *dev = NULL;
	unsigned short devid = 0,mask = 0x3fe0;
	int family = PIC10_PIC12_PIC16Family;
/*	
	// VppFirst
	cmd14.enterProgramVerifyMode(PICProgrammer::VppBeforeVdd);
	cmd14.loadConfiguration(0);
	cmd14.incrementAddress(6);
	cmd14.readDataFromProgramMemory(&devid);
	cmd14.exitProgramVerifyMode(PICProgrammer::VddBeforeVpp);
	
	if (!devid){
		// VddFirst
		cmd14.enterProgramVerifyMode(PICProgrammer::VddBeforeVpp);
		cmd14.loadConfiguration(0);
		cmd14.incrementAddress(6);
		cmd14.readDataFromProgramMemory(&devid);
		cmd14.exitProgramVerifyMode(PICProgrammer::VppBeforeVdd);
	}
	
	if (!devid){
		PIC18Command cmd18(*prog);
		unsigned char w[2];
		cmd18.enterProgramVerifyMode();
		cmd18.settblptr(0x3ffffe);
		cmd18.readCodeMemory(w,2);
		cmd18.exitProgramVerifyMode();
		devid = w[1]<<8 | w[0];
		family = PIC18Family;
		mask = 0xffe0;
	}
	
	if (!devid){
		PIC24Command cmd24(*prog);
		unsigned long w[2];
		cmd24.reset();
		cmd24.readDataConfigMemory(0xff0000,w,2);
		devid = w[0];
		family = PIC24Family | dsPICFamily;
		mask = 0xffff;
	}
	*/
	if(!devid){
		/*
		PSoC1Comand cmd24(*prog);
		ここでdevice id codeを読み出して、non-zeroならその値をreturnする
		*/
	}
	
	if(!devid){
		/*
		PSoC3Command cmdswd(*prog);
		*/
	}
	
	return searchDeviceFromID(devid,mask,family);
}

PICDevice* PICDevice::searchDeviceFromName(const char *name)
{
	if (name){
		PICDevice *dev = m_devices;
//		if (strncasecmp(name,"pic",3) == 0) name += 3; // PICxxFxxx -> xxFxxx
//		if (strncasecmp(name,"dspic",5) == 0) name += 5; // dsPICxxFxxx -> xxFxxx
		if (strncasecmp(name,"cy8c",4) == 0) name += 4; // CY8Cxxxxx -> xxxxx
		while (dev){
			char *p = dev->GetName();
//			if (strncasecmp(p,"pic",3) == 0) p += 3; // PICxxFxxx -> xxFxxx
//			if (strncasecmp(p,"dspic",5) == 0) p += 5; // dsPICxxFxxx -> xxFxxx
			if (strncasecmp(p,"cy8c",4) == 0) p += 4; // CY8Cxxxxx -> xxxxx
			if (strcasecmp(p,name) == 0)
				return dev;
			dev = dev->m_pNext;
		}
	}
	return 0;
}

void PICDevice::dumpAllDevices(FILE *fout)
{
	PICDevice *dev = m_devices;
	while (dev){
  		fprintf(stderr, "%s ", dev->GetName());
		dev=dev->m_pNext;
	}
	fprintf(stderr, "\n");
}
#if 0
void PICDevice::dumpProperties(FILE *fout)
{
	fprintf(fout, "Begin %s\n", m_pName);
	if (m_pDefault){
		fprintf(fout, "  " DEFAULTSTR  "%s\n", m_pDefault->m_pName);
		// todo. print differ.
	}
	else{
		fprintf(fout, "  " IDWORDSTR   "%x\n", m_nIDword);
//		fprintf(fout, "  " IDADDRSTR   "%x\n", m_nIDAddr);
//		fprintf(fout, "  " PWUPTYPESTR "%s\n", powertype_str(m_ePowerUp));
//		fprintf(fout, "  " PMMASKSTR   "%x\n", m_nPMMask);
		fprintf(fout, "  " PMSIZESTR   "%d\n", m_nPMSize);
//		fprintf(fout, "  " EESIZESTR   "%d\n", m_nEEMemSize);
//		fprintf(fout, "  " CFGDSPSZSTR "%d\n", m_nConfDspSize);
//		fprintf(fout, "  " CFGADDR1STR "%x\n", m_nConfAddr1);
//		fprintf(fout, "  " CFGADDR2STR "%x\n", m_nConfAddr2);
//		fprintf(fout, "  " CFGMSK1STR  "%x\n", m_nConfMask1);
//		fprintf(fout, "  " CFGMSK2STR  "%x\n", m_nConfMask2);
//		fprintf(fout, "  " CALBADDRSTR);
/*		for (int i=0;i<8;i++)
			fprintf(fout, " %x", m_aCalibAddrs[i]);
		fprintf(fout, "\n");
		fprintf(fout, "  " CALBMASKSTR);
		for (int i=0;i<8;i++)
			fprintf(fout, " %x", m_aCalibMasks[i]);
		fprintf(fout, "\n");*/
//		fprintf(fout, "  " PRGTYPESTR  "%s\n", progamtype_str(m_eProgType));
//		fprintf(fout, "  " PRGTYPECSTR "%s\n", progamtype_str(m_eProgTypeC));
//		fprintf(fout, "  " PRGWAITSTR  "%d\n", m_nProgWait);
//		fprintf(fout, "  " PRGWAITCSTR "%d\n", m_nProgWaitC);
//		fprintf(fout, "  " PRGWAITDSTR "%d\n", m_nProgWaitD);
//		fprintf(fout, "  " PRGLATCHSTR "%d\n", m_nProgLatch);
//		fprintf(fout, "  " ERSTYPESTR  "%s\n", erasetype_str(m_eEraseType));
//		fprintf(fout, "  " ERSWAITSTR  "%d\n", m_nEraseWait);
	}
	if(m_bPIC18){
		// todo. print pic18's properties.
	}
	fprintf(fout, "End\n#\n");
}
/*
const char* PICDevice::powertype_str(PowerSequence type)
{
	const char* result = "";
	if(type == VddFirst){
		result = VDDVPPSTR;
	}else if(type == VppFirst){
		result = VPPVDDSTR;
	}
	return result;
}

const char* PICDevice::progamtype_str(ProgammingType type)
{
	const char* result = "";
	if(type == Internal08){
		result = INT08STR;
	}else if(type == Internal18){
		result = INT08STR;
	}else if(type == External08_e){
		result = EXT08STR;
	}else if(type == External18_17){
		result = EXT18STR;
	}else if(type == External18_a){
		result = EXT18ASTR;
	}
	return result;
}

const char* PICDevice::erasetype_str(EraseType type)
{
	const char* result = "";
	if(type == BulkErase_F509){
		result = BESF509STR;
	}else if(type == BulkErase_F519){
		result = BESF519STR;
	}else if(type == BulkErase_F675){
		result = BESF675STR;
	}else if(type == BulkErase_F683){
		result = BESF683STR;
	}else if(type == BulkErase_F84){
		result = BESF84STR;
	}else if(type == BulkErase_F648A){
		result = BESF648ASTR;
	}else if(type == ChipErase_F88){
		result = CESF88STR;
	}else if(type == ChipErase_F77){
		result = CESF77STR;
	}
	return result;
}
*/

#endif
