// PSoC1Command.h
//    2012.2 aasoukai128


#ifndef PSoC1COMMAND_H
#define PSoC1COMMAND_H

#include "PICProgrammer.h"

class PSoC1Command {
	PICProgrammer &m_prog;
	int m_increment;
	
public:
	
	PSoC1Command(PICProgrammer &prog);
	~PSoC1Command();
	
	// Primitive PSoC1 Command
	void writeCommand(unsigned char addr, unsigned char cmd);
	void writeNullCommand(void);
	void readData(unsigned char addr, unsigned char *rbuf);
	void writeData(unsigned char addr, unsigned char data);
	
	// Read/Write Sequence
	int wordWriteCycle(int cmd,int begin,int end,int wai,const unsigned short *wbuf,int count);
	int wordReadCycle(int cmd,unsigned short *rbuf,int count);
	
	// Enter/Exit Program Verify Mode
	void enterDebugMode(void);
	void exitDebugMode(void);
private:
};

#endif
