// PICBurnEnv.h
//    2007.6 Y.Senta
//    2008.5 arms22

#ifndef _PICBURNENV_H_
#define _PICBURNENV_H_

class PICDevice;
class PICProgrammer;
class PICStrategy;

class PICBurnCmd
{
public:
	bool m_bCheck;
	bool m_bErase;
	bool m_bWrite;
	bool m_bVerify;
	bool m_bRead;
	bool m_bTest;
	
	bool m_bLVP;
	bool m_bQuite;
	
	bool m_bAdvance;
	
	char *m_pBPS;
	char *m_pDeviceName;
	char *m_pChipName;
	char *m_pNewBPS;
	char *m_pInputHEXPath;
	char *m_pOutputHEXPath;
	char *m_pProgrammerName;
	
	PICBurnCmd(){
		m_bCheck=false;
		m_bErase=false;
		m_bWrite=false;
		m_bVerify=false;
		m_bRead=false;
		m_bTest=false;
		
		m_bLVP=false;
		m_bQuite=false;
		
		m_bAdvance=true;
		
		m_pDeviceName=NULL;
		m_pBPS=NULL;
		m_pChipName=NULL;
		m_pNewBPS=NULL;
		m_pInputHEXPath=NULL;
		m_pOutputHEXPath=NULL;
		m_pProgrammerName=NULL;
	};
	
	~PICBurnCmd(){
		if (m_pDeviceName)
			delete [] m_pDeviceName;
		if (m_pBPS)
			delete [] m_pBPS;
		if (m_pChipName)
			delete [] m_pChipName;
		if (m_pNewBPS)
			delete [] m_pNewBPS;
		if (m_pInputHEXPath)
			delete [] m_pInputHEXPath;
		if (m_pOutputHEXPath)
			delete [] m_pOutputHEXPath;
	}
	
	void DBDump();
	bool ChipAccessCmd();
	bool isCommand();
	PICProgrammer* CreateProgrammer();
};

class PICBurnEnv
{
	PICProgrammer *m_pProgrammer;
	PICStrategy *m_pStrategy;
	
public:
	bool Init(const char *);
	void Final();
	bool DoCmd(PICBurnCmd &);
	
	PICBurnEnv() : m_pProgrammer(NULL), m_pStrategy(NULL) {}
	~PICBurnEnv(){}
	
	void DumpDevice();
};

#endif
