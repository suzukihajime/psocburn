// PSoC5Command.h
//    2012.8 aasoukai128

#ifndef PSoC5COMMAND_H
#define PSoC5COMMAND_H

#include "PICProgrammer.h"

class PSoC5Command {
	PICProgrammer &m_prog;
	int m_increment;
	
public:
	
	PSoC5Command(PICProgrammer &prog);
	~PSoC5Command();
	
	// Primitive PSoC5/5 Command
	bool loadDataForApacc(unsigned int addr,unsigned int data);
	bool pollDataInApacc(unsigned int addr,unsigned int data);
	bool setDMAAccessWidth(int width);
	bool readDataFromApacc(unsigned int addr,unsigned int *rbuf);
	bool readMultipleBytesFromApacc(unsigned int addr,unsigned char *rbuf, int length);
	void readDataFromDpacc(unsigned int *rbuf);
	void beginCommand(void);
	void endCommand(void);
	void switchToSWD(void);
	
	// Read/Write Sequence
	int wordWriteCycle(int cmd,int begin,int end,int wai,const unsigned char *wbuf,int count);
	int wordReadCycle(int cmd,unsigned char *rbuf,int count);
	
	// Enter/Exit Program Verify Mode
	bool enterDebugMode(void);
	void exitDebugMode(void);
	void configureNonVolatileLatch(void);
private:
};

#endif
