// utils.h
//    2007.6 Y.Senta

#ifndef _COMMON_H_
#define _COMMON_H_

extern int g_nDebugLevel;
extern int  dbprintf(int, const char *, ...);
extern char *setstring(const char *);
extern char *chopstr(char *);
extern bool gethexbyte(int *, const char *, int);
extern char *cfgstrcmp(char *,const char *);

#endif
