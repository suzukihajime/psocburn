// PSoC5Command.cc
//    2012.8 aasoukai128


#include "PSoC5Command.h"

/* for debug */
#include "stdio.h"

PSoC5Command::PSoC5Command(PICProgrammer &prog)
  : m_prog(prog)
{
}

PSoC5Command::~PSoC5Command()
{
}

bool PSoC5Command::loadDataForApacc(unsigned int addr, unsigned int data)
{
	unsigned int ret = 0;
	m_prog.commandswd(PICProgrammer::apacc_addr_write, addr);
	ret |= !m_prog.commandswd(PICProgrammer::apacc_data_write, data);
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

bool PSoC5Command::pollDataInApacc(unsigned int addr, unsigned int data)
{
	int cnt = 1000, ret;
	m_prog.commandswd(PICProgrammer::apacc_addr_write, addr);
	m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	data <<= 16;
	do{
		if((ret = m_prog.commandswd(PICProgrammer::apacc_data_read, 0) & 0x00ff0000) == data){
			return true;
		}/* wait for a second */
//		fprintf(stderr, "waiting ret = %d\n", ret);
	} while(--cnt > 0);
	return false;
}

bool PSoC5Command::setDMAAccessWidth(int width)
{
	switch(width){
	  case 1:
		return(m_prog.commandswd(PICProgrammer::apacc_ctrlstat_write, 0x22000000));
	  case 2:
		return(m_prog.commandswd(PICProgrammer::apacc_ctrlstat_write, 0x22000001));
	  case 4:
		return(m_prog.commandswd(PICProgrammer::apacc_ctrlstat_write, 0x22000002));
	  default:
		return(false);
	}
}

bool PSoC5Command::readDataFromApacc(unsigned int addr, unsigned int *rbuf)
{
	bool ret = 0;
	ret = !m_prog.commandswd(PICProgrammer::apacc_addr_write, addr);
	m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	*rbuf = m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

bool PSoC5Command::readMultipleBytesFromApacc(unsigned int addr, unsigned char *rbuf, int length)
{
	unsigned int ret = 0, count;
	unsigned int *tempbuf = new unsigned int[length];
	unsigned int *ptr;
	
	ret |= !m_prog.commandswd(PICProgrammer::apacc_addr_write, addr);
	ret |= !m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	m_prog.beginCommand();
	count = length;
	while(count--) {
		m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	}
	m_prog.endCommand();
	m_prog.readWord(tempbuf, length);
	ptr = tempbuf;
	while(length--) {
		*rbuf++ = (unsigned char)*ptr++;
	}
	delete [] tempbuf;
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

void PSoC5Command::readDataFromDpacc(unsigned int *rbuf)
{
	*rbuf = m_prog.commandswd(PICProgrammer::dpacc_id_read, 0);
	return;
}

void PSoC5Command::beginCommand(void)
{
	m_prog.beginCommand();
	return;
}

void PSoC5Command::endCommand(void)
{
	m_prog.endCommand();
	return;
}

void PSoC5Command::switchToSWD(void)
{
	m_prog.switchjtagtoswd();
	return;
}

/*
int PSoC5Command::wordWriteCycle(int cmd,int begin,int end,int wai,const unsigned char *wbuf,int count)
{
	return 0;
}

int PSoC5Command::wordReadCycle(int cmd,unsigned char *rbuf,int count)
{
	return 0;
}

void PSoC5Command::configureNonVolatileLatch(void)
{
}
*/
bool PSoC5Command::enterDebugMode(void)
{
	unsigned short cnt = 0;
	bool ret = 0;
	
	m_prog.powerOn();
	m_prog.reset(PICProgrammer::NormalRelease, PICProgrammer::NegativeReset);
	cnt = 10;
	while(m_prog.acquireswd() && --cnt > 0) {}
	if(cnt == 0) {
		return false;
	}
	// debug port acquire succeeded
	loadDataForApacc(0x00050203, 0x00000020);
	switchToSWD();
	ret |= !m_prog.commandswd(PICProgrammer::dpacc_config_write, 0x50000000);
	ret |= !m_prog.commandswd(PICProgrammer::dpacc_select_write, 0x00000000);
	ret |= !m_prog.commandswd(PICProgrammer::apacc_ctrlstat_write, 0x22000002);
	ret |= !loadDataForApacc(0xe000edf0, 0xa05f0003);	// Halt CPU, Enable Debug-on-chip (DoC) access
	ret |= !loadDataForApacc(0x4008000c, 0x00000002);	// Halt CPU
	ret |= !loadDataForApacc(0x400043a0, 0x000000bf);	// Enable individual sub-system of chip
	ret |= !loadDataForApacc(0x40004200, 0x00000002);	// IMO set to 24 MHz
	if(!ret) {
		return true;		// entering debug mode succeeded
	} else {
		return false;
	}
}

void PSoC5Command::exitDebugMode(void)
{
	m_prog.reset(PICProgrammer::NormalRelease, PICProgrammer::NegativeReset);
	m_prog.powerOn();
	m_prog.delay(10*1000*1000);	// > 10ms
}
/*
void PSoC5Command::configureDevice(void)
{
	
}
*/