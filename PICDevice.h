// PICDevice.h
//    2007.6 Y.Senta
//    2008.5 arms22
//    2011.12 aasoukai128

#ifndef PICDEVICE_H
#define PICDEVICE_H

#include <stdio.h>

#include "PICMemory.h"
#include "PICStrategy.h"
#include "PICProgrammer.h"

class PICDevice
{
public:
	/*
	enum PowerSequence {
		VddFirst,
		VppFirst,
	};
	
	enum ProgammingType {
		Internal08,
		Internal18,
		External08_e,
		External18_17,
		External18_a,
	};
	
	enum EraseType {
		BulkErase_F509,
		BulkErase_F519,
		BulkErase_F675,
		BulkErase_F683,
		BulkErase_F84,
		BulkErase_F648A,
		ChipErase_F88,
		ChipErase_F77,
	};
	
	enum EEWriteType18 {
		PollWRbit,
		FixWaitTime,
	};
	
	enum {
		NVMCONValueEraseAllMemory = 0,
		NVMCONValueWriteOneRowToCode = 1,
		NVMCONValueWriteOneRowToData = 2,
		NVMCONValueWriteOneWordToConfiguration = 3,
	};
	
	enum {
		WriteLatchsForCode = 0,
		WriteLatchsForData = 1,
	};
	*/
	enum {
		PIC10_PIC12_PIC16Family = 1,
		PIC18Family = 2,
		PIC24Family = 4,
		dsPICFamily = 8,
		PSoC1Family = 16,
		PSoC3Family = 32,
		PSoC5Family = 64,
	};
	
private:
	PICDevice*		m_pNext;
	PICDevice*		m_pDefault;
	char*			m_pName;
	int				m_nIDword;
//	unsigned long	m_nIDAddr;
//	PowerSequence	m_ePowerUp;
//	int				m_nPMMask;
	int				m_nPMSize;
//	int				m_nDMSize;
	int				m_nConfigSize;
	int				m_nNvlSize;
	int				m_nBlockSize;
	int				m_nArraySize;
	int				m_nEccBlockSize;
	int				m_nSiliconRev;
//	unsigned long	m_nConfAddr1;
//	unsigned long	m_nConfAddr2;
//	int				m_nConfMask1;
//	int				m_nConfMask2;
//	unsigned long	m_aCalibAddrs[8];
//	int				m_aCalibMasks[8];
//	ProgammingType	m_eProgType;
//	ProgammingType	m_eProgTypeC;
//	int				m_nProgWait;
//	int				m_nProgWaitD;
//	int				m_nProgWaitC;
//	int				m_nProgLatch;
//	EraseType		m_eEraseType;
//	int				m_nEraseWait;
	
//	int				m_nWriteBufSize;
//	int				m_aConfigMask[14];
//	int				m_aConfigValue[14];
//	int				m_nEraseCommand;
//	EEWriteType18	m_eEEWriteType;
//	int				m_nPanelNum;
//	int				m_nPanelSize;
//	int				m_nProgWaitID;
	
//	int				m_WriteLatches[2];
//	int				m_NVMCONValues[4];
//	int				m_nExecCodeSize;
	
	int				m_productFamily;
	
	static PICDevice* m_devices;
	
public:
	
	PICDevice(const char *);
	~PICDevice();
	
	char *GetName(){
		return m_pName;
	}
	int GetProgSize(){
		return m_nPMSize;
	}
/*	int GetDataSize(){
		return m_nDMSize;
	}*/
	int GetConfSize(){
		return m_nConfigSize;
	}
	int GetNvlSize(){
		return m_nNvlSize;
	}
	int GetBlockSize(){
		return m_nBlockSize;
	}
	int GetArraySize() {
		return m_nArraySize;
	}
	int GetEccBlockSize() {
		return m_nEccBlockSize;
	}
	int GetSiliconRev() {
		return m_nSiliconRev;
	}
/*	unsigned long GetIDAddr(){
		return m_nIDAddr;
	}*/
/*	unsigned long GetCalibAddr(int index){
		return m_aCalibAddrs[index&7];
	}*/
/*	int GetCalibMask(int index){
		return m_aCalibMasks[index&7];
	}*/
/*	int GetEraseWait(){
		return m_nEraseWait;
	}*/
/*	int GetProgWait(){
		return m_nProgWait;
	}*/
/*	int GetProgWaitC(){
		return m_nProgWaitC;
	}*/
/*	int GetProgWaitD(){
		return m_nProgWaitD;
	}*/
/*	int GetProgLatch(){
		return m_nProgLatch;
	}*/
/*	PowerSequence GetPowerSequence(){
		return m_ePowerUp;
	}
	EraseType GetEraseType(){
		return m_eEraseType;
	}*/
	int GetDeviceID(){
		return m_nIDword;
	}
/*	int GetPMMask(){
		return m_nPMMask;
	}*/
/*	int GetConfMask1(){
		return m_nConfMask1;
	}*/
/*	int GetConfMask2(){
		return m_nConfMask2;
	}*/
/*	ProgammingType GetProgType(){
		return m_eProgType;
	}
	ProgammingType GetProgTypeC(){
		return m_eProgTypeC;
	}*/
/*	unsigned long GetConfAddr1(){
		return m_nConfAddr1;
	}
	unsigned long GetConfAddr2(){
		return m_nConfAddr2;
	}*/
	int productFamily(void){
		return m_productFamily;
	}
/*	int GetWriteBufferSize(){
		return m_nWriteBufSize;
	}*/
/*	int* GetConfigMask(){
		return m_aConfigMask;
	}
	int* GetConfigValue(){
		return m_aConfigValue;
	}
	int GetEraseCommand(){
		return m_nEraseCommand;
	}*/
/*	EEWriteType18 GetEEWriteType(){
		return m_eEEWriteType;
	}*/
/*	int GetPanelNum(){
		return m_nPanelNum;
	}
	int GetPanelSize(){
		return m_nPanelSize;
	}*/
/*	int GetProgWaitID(){
		return m_nProgWaitID;
	}*/
/*	int GetWriteLatches(int index){
		return m_WriteLatches[index&1];
	}
	int GetNVMCONValue(int index){
		return m_NVMCONValues[index&3];
	}
	int GetExecCodeSize(){
		return m_nExecCodeSize;
	}*/
	PICStrategy* createStrategy(PICProgrammer *prog);
	static bool loadDeviceList(FILE *fin);
	bool load(FILE *);
	static PICDevice* searchDeviceFromID(unsigned short devid,unsigned short devid_mask,int family);
	static PICDevice* deviceAutoDetect(PICProgrammer *prog);
	static PICDevice* searchDeviceFromName(const char *name);
	static void dumpAllDevices(FILE *fout);
	void dumpProperties(FILE *fout);
	
private:
//	const char* powertype_str(PowerSequence type);
//	const char* progamtype_str(ProgammingType type);
//	const char* erasetype_str(EraseType type);
};

#endif
