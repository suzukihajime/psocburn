// properties.h
//    2007.6 Y.Senta
//    2008.4.24 arms22

#ifndef _PROPERTIES_H_
#define _PROPERTIES_H_

#define COMMENTCHAR '#'
#define DEFSTARTSTR "Begin "
#define DEFENDSTR	"End"

#define DEFAULTSTR  "Default "
#define IDWORDSTR   "DeviceIDWord "
#define IDADDRSTR   "UserIDAddress "
#define PWUPTYPESTR "PowerUPType "
#define PMMASKSTR   "ProgramMemoryMask "
#define PMSIZESTR   "ProgramMemorySize "
#define CFGSIZESTR  "ConfigMemorySize "
#define BLKSIZESTR  "BlockSize "
#define	ECCSIZESTR  "EccBlockSize "
#define SILREVSTR   "SiliconRev "
#define NVLSIZESTR  "NVLSize "
#define EESIZESTR   "EEDataMemorySize "
#define CFGDSPSZSTR "ConfigDispSize "
#define CFGADDR1STR "ConfigAddress1 "
#define CFGADDR2STR "ConfigAddress2 "
#define CFGMSK1STR  "ConfigMask1 "
#define CFGMSK2STR  "ConfigMask2 "
#define CALBADDRSTR "CalibAddress "
#define CALBMASKSTR "CalibMask "
#define PRGTYPESTR  "ProgrammingType "
#define PRGTYPECSTR "ProgrammingTypeC "
#define PRGWAITSTR  "ProgrammingWait "
#define PRGWAITCSTR "ProgrammingWaitC "
#define PRGWAITDSTR "ProgrammingWaitD "
#define PRGLATCHSTR "ProgrammingLatch "
#define ERSTYPESTR  "EraseType "
#define ERSWAITSTR  "EraseWait "

#define VDDVPPSTR   "VddBeforeVpp"
#define VPPVDDSTR   "VppBeforeVdd"

#define INT08STR    "Internal08"
#define INT18STR    "Internal18"
#define EXT08STR    "External08"
#define EXT18STR    "External18"
#define EXT18ASTR   "External18A"

#define BESF509STR  "BulkEraseF509"
#define BESF519STR  "BulkEraseF519"
#define BESF675STR  "BulkEraseF675"
#define BESF683STR  "BulkEraseF683"
#define BESF84STR   "BulkEraseF84"
#define BESF648ASTR "BulkEraseF648A"
#define CESF88STR   "ChipEraseF88"
#define CESF77STR   "ChipEraseF77"

#define WRBFSZSTR   "WriteBufferSize "
#define CFGMSKSTR   "ConfigMask "
#define CFGVALSTR   "ConfigValue "
#define ERSCMDSTR   "EraseCommand "
#define EEWTYPSTR   "EEWriteType "
#define PNLNUMSTR   "PanelNum "
#define PNLSIZSTR   "PanelSize "
#define PRGWAIPSTR  "ProgrammingWaitP "
#define PRGWAIIDSTR "ProgrammingWaitID "

#define ECMD80STR   "Command80"
#define ECMD3F8FSTR "Command3F8F"

#define PLLWRBITSTR "PollWRbit"
#define FXWITTMSTR  "FixWaitTime"

#define WRLATSTR    "WriteLatches "
#define NVCOMVALSTR "NVMCONValues "
#define EXECSIZESTR "ExecutiveCodeSize "

#endif
