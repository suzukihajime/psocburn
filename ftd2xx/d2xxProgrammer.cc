#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "d2xxProgrammer.h"

#define TDLY2			(3)		// > 1us
#define TDIS			(220)	// > 100us
#define TDLY6			(220)	// > 100us

// TXD(1)  D0 o out
// RXD(5)  D1 i in
// RTS(3)  D2 o clk
// CTS(11) D3 o #vdd
// DTR(2)  D4 o #vpp


/* for debug */
#include "stdio.h"


d2xxProgrammer::d2xxProgrammer()
	: m_ftHandle(0)
	, m_iobuf(pin_in,pin_out,pin_clk)
	, m_queuingMode(0)
{
}

d2xxProgrammer::~d2xxProgrammer()
{
	close();
}

bool d2xxProgrammer::open(const char *port)
{
	FT_STATUS	ftStatus;
	
	ftStatus = FT_Open(0, &m_ftHandle);
	if(ftStatus != FT_OK) {
		/* 
		   This can fail if the ftdi_sio driver is loaded
		   use lsmod to check this and rmmod ftdi_sio to remove
		   also rmmod usbserial
		*/
		if(ftStatus == FT_DEVICE_NOT_FOUND){
			fprintf(stderr,"ftdi device not found\n");
		}else if(ftStatus == FT_DEVICE_NOT_OPENED){
			fprintf(stderr,
					"ftdi device not opend, unload ftdi_sio.\n"
					"linux - rmmod ftdi_sio\n"
					"osx - sudo kextunload /System/Library/Extensions/FTDIUSBSerialDriver.kext\n");
		}else{
			fprintf(stderr,"FT_Open failed = %d\n", ftStatus);
		}
		return false;
	}
	
	ftStatus = FT_SetBitMode(m_ftHandle, 0xff & ~(pin_in), 0x04);
	if(ftStatus != FT_OK) {
		fprintf(stderr,"FT_SetBitMode failed = %d\n");
		return false;
	}
	init_port();
	
//	ftStatus = FT_SetDivisor(m_ftHandle, 0);
	ftStatus = FT_SetBaudRate(m_ftHandle,2000000);
	//ftStatus = FT_SetBaudRate(m_ftHandle, 57600);
	if(ftStatus != FT_OK) {
		fprintf(stderr,"FT_SetBaudRate failed = %d\n");
		return false;
	}
	
	ftStatus = FT_SetLatencyTimer(m_ftHandle,2);
	if(ftStatus != FT_OK) {
		fprintf(stderr,"FT_SetLatencyTimer failed = %d\n");
		return false;
	}
	
	ftStatus = FT_Purge(m_ftHandle,FT_PURGE_RX|FT_PURGE_TX);
	if(ftStatus != FT_OK) {
		fprintf(stderr,"FT_Purge failed = %d\n");
		return false;
	}
	return true;
}

void d2xxProgrammer::init_port(void)
{
	m_iobuf.set_port(pin_vdd|pin_vpp,
					 pin_vdd|pin_vpp);
	execute_queue();
}

void d2xxProgrammer::close()
{
	if(m_ftHandle != NULL) {
		init_port();
		FT_Close(m_ftHandle);
		m_ftHandle = NULL;
	}
}

void d2xxProgrammer::powerOn(void)
{
	power_on(pin_vdd,pin_vpp);
//	reset(NormalRelease, polarity);
}

void d2xxProgrammer::powerOff(void)
{
	power_off(pin_vpp,pin_vdd);
}

void d2xxProgrammer::reset(unsigned char release, unsigned char polarlity)
{
	m_iobuf.set_port(pin_rst,0);
	execute_queue();
	delay(3000000);
	if(release == NormalRelease) {
		m_iobuf.set_port(pin_rst,pin_rst);
		execute_queue();	
	}
}

void d2xxProgrammer::delay(unsigned long ns)
{
#if 0
	struct timeval 	tv1,tv2;
	gettimeofday(&tv1, 0);
	tv2.tv_sec 	= tv1.tv_sec;
	tv2.tv_usec = tv1.tv_usec + ((ns + 999) / 1000) + 1;
	if (tv2.tv_usec < tv1.tv_usec)
		tv2.tv_sec++;
	do {
		gettimeofday(&tv1, 0);
		if (tv1.tv_sec >  tv2.tv_sec 	||
			tv1.tv_sec == tv2.tv_sec && tv1.tv_usec >= tv2.tv_usec)
			break;
	} while(1);
#else
	usleep((ns+999)/1000);
#endif
}

/*
unsigned short d2xxProgrammer::command(int cmd,unsigned short data)
{
	add_command(cmd,data);
	if(!m_queuingMode){
		execute_queue();
		switch(cmd){
		case read_prog:
		case read_data:
			return readOneWord();
		}
	}
	return 0;
}

unsigned char d2xxProgrammer::command18(int cmd,unsigned short data)
{
	add_command18(cmd,data);
	if(!m_queuingMode){
		execute_queue();
		switch(cmd){
		case shift_out:
		case tread:
		case tread_inc:
		case tread_dec:
		case inc_tread:
			return readOneByte();
		}
	}
	return 0;
}

unsigned short d2xxProgrammer::command30(int cmd,unsigned long data)
{
	add_command30(cmd,data);
	if(!m_queuingMode){
		execute_queue();
		if(cmd == regout)
			return readOneVISI();
	}
	return 0;
}

unsigned char d2xxProgrammer::command24(int cmd,unsigned char addr,unsigned char data)
{	// for PSoC1
	// nothing to do
	return 0;
}
*/
unsigned int d2xxProgrammer::commandswd(int cmd,unsigned int data)
{
	unsigned char ack;
	add_commandswd(cmd,data);
	if(!m_queuingMode){
		execute_queue();
		readAck(&ack);
		if(ack == 0x01){
			switch(cmd){
			  case dpacc_id_read:
			  case apacc_data_read:
				return readOneWord();
			  default:
				return 1;
			}
		}else{
			return 0;
		}
	}
	return 0;
}

unsigned int d2xxProgrammer::acquireswd(void)
{
	unsigned char ack[5];
	int i;
	add_acquireswd();
	if(!m_queuingMode){
		execute_queue();
		for(i = 0; i < 5; i++){
			readAck(&ack[i]);
		}
//		fprintf(stderr, "%d, %d, %d, %d, %d, %d\n", ack[0], ack[1], ack[2], ack[3], ack[4]);
		if((ack[1] != 0x01 && ack[2] != 0x01) || ack[3] != 0x01 || ack[4] != 0x01) {
			return 1;
		}
	}
	return 0;
}

unsigned int d2xxProgrammer::switchjtagtoswd(void)
{
	add_jtagtoswd();
	if(!m_queuingMode){
		execute_queue();
		return 1;
	}
	return 0;
}

unsigned char d2xxProgrammer::commandissp(int cmd, unsigned char addr, unsigned char data)
{
	add_commandissp(cmd, addr, data);
	if(!m_queuingMode){
		execute_queue();
		switch(cmd)
		{
		  case issp_data_read:
			return readOneByte();
		}
	}
	return 0;
}

unsigned char d2xxProgrammer::acquireissp(void)
{
	add_acquireissp();
	if(!m_queuingMode){
		execute_queue();
	}
	return 0;
}

void d2xxProgrammer::power_on(int fst,int snd)
{
	m_iobuf.set_port(fst,0);
	m_iobuf.set_stbl(31);
	m_iobuf.set_port(snd,0);
	m_iobuf.set_stbl(31);
	execute_queue();
}

void d2xxProgrammer::power_off(int fst,int snd)
{
	m_iobuf.set_port(fst,fst);
	m_iobuf.set_stbl(31);
	m_iobuf.set_port(snd,snd);
	m_iobuf.set_stbl(31);
	execute_queue();
}

int d2xxProgrammer::send_and_recv(unsigned char *rwbuf,int size)
{
	FT_STATUS ftStatus;
	DWORD ret;
#ifdef DEBUG_FTDI
	struct timeval tv1,tv2,tv3;
	gettimeofday(&tv1, 0);
#endif
	if(((ftStatus = FT_Write(m_ftHandle,rwbuf,size,&ret)) != FT_OK)
	   || (ret != size)) {
		fprintf(stderr,"couldn't write to ftdi device: %d", ftStatus);
		FT_Close(m_ftHandle);
		exit(-1);
	}
#ifdef DEBUG_FTDI
	gettimeofday(&tv2, 0);
#endif
	if(((ftStatus = FT_Read(m_ftHandle,rwbuf,size,&ret)) != FT_OK)
	   || (ret != size)) {
		fprintf(stderr,"couldn't read from ftdi device: %d", ftStatus);
		FT_Close(m_ftHandle);
		exit(-1);
	}
#ifdef DEBUG_FTDI
	gettimeofday(&tv3, 0);
	unsigned long t12,t23;
	if(tv1.tv_sec == tv2.tv_sec){
		t12 = tv2.tv_usec - tv1.tv_usec;
	}else{
		t12 = tv2.tv_usec + ((tv2.tv_sec - tv1.tv_sec - 1) * 1000000) + (1000000-tv1.tv_usec);
	}
	if(tv2.tv_sec == tv3.tv_sec){
		t23 = tv3.tv_usec - tv2.tv_usec;
	}else{
		t23 = tv3.tv_usec + ((tv3.tv_sec - tv2.tv_sec - 1) * 1000000) + (1000000-tv2.tv_usec);
	}
	fprintf(stderr,"%.3f %.3f\n",(double)t12/1000.0,(double)t23/1000);
#endif
	return ret;
}

#if 0
void d2xxProgrammer::add_command(int cmd,unsigned short data)
{
	m_iobuf.set_data(cmd,6);
	m_iobuf.set_stbl(TDLY2);
	switch(cmd){
	case read_prog:
	case read_data:
		m_iobuf.set_rdpt(1);	// 0xxxxxxxxxxxxxx0
	case load_config:
	case load_prog:
	case load_data:
		data = (data & 0x3fff) << 1; // 0xxxxxxxxxxxxxx0
		m_iobuf.set_data(data,16);
		m_iobuf.set_stbl(TDLY2);
		break;
	case end_prog_a:
	case end_prog_e:
	case end_prog_17:
		m_iobuf.set_stbl(TDIS);
		break;
	}
}

void d2xxProgrammer::add_command18(int cmd,unsigned short data)
{
	switch(cmd){
	case shift_in:
		goto _set_;
	case shift_out:
	case tread:
	case tread_inc:
	case tread_dec:
	case inc_tread:
		m_iobuf.set_rdpt(12);
	case twrite:
	case twrite_inc:
	case twrite_dec:
	case twrite_start:
		goto _set_;
	case nop_prog:
		m_iobuf.set_data(0,3);
		m_iobuf.set_port(pin_clk,pin_clk);
		break;
	case nop_erase:
		m_iobuf.set_data(0,4);
		break;
	case end_prog_erase:
		m_iobuf.set_port(pin_clk, 0);
		m_iobuf.set_stbl(TDLY6);
		m_iobuf.set_data(0,16);
		break;
	default:
	_set_:
		m_iobuf.set_data(cmd,4);
		m_iobuf.set_data(data,16);
		break;
	}
}

void d2xxProgrammer::add_command30(int cmd,unsigned long data)
{
	if(cmd == regout)
		m_iobuf.set_rdpt(12);
	m_iobuf.set_data(cmd,4);
	m_iobuf.set_data(data,24);
}


void d2xxProgrammer::add_command24(int cmd,unsigned char addr,unsigned char data)
{
	/* PSoC1 Command */
	/*
	switch(cmd)
	{
	  case pread_data:
	  case pread_status:
		m_iobuf.set_rdpt(13);
	  case pwrite_data:
	  case pwrite_addr:
		m_iobuf.set_data(cmd,3);
		m_iobuf.set_data(addr,8);
	}
	m_iobuf.set_data(
	*/
}
#endif

void d2xxProgrammer::add_commandswd(int cmd,unsigned int data)
{
	switch(cmd)
	{
	  case dpacc_id_read:
	  case apacc_data_read:
		if(!m_queuingMode) {
			m_iobuf.set_rdpt(9);
		}
		m_iobuf.set_rdpt(12);
		m_iobuf.set_data(cmd,8);
		m_iobuf.set_data(0,41);
		break;
	  case apacc_addr_write:
	  case apacc_data_write:
	  case apacc_ctrlstat_write:
	  case dpacc_rb_write:
	  case dpacc_config_write:
	  case dpacc_select_write:
		m_iobuf.set_rdpt(9);
		m_iobuf.set_data(cmd,8);
		m_iobuf.set_data(0,5);
		m_iobuf.set_data(data,32);
		m_iobuf.set_data(get_parity(data),1);
		m_iobuf.set_data(0,3);
		break;
	  default:
		m_iobuf.set_data(0,50);		// swd interface reset
		break;
	}
}

void d2xxProgrammer::add_acquireswd(void)
{
	m_iobuf.set_port(pin_out | pin_rst,pin_out);
	m_iobuf.set_data(0,100);
	m_iobuf.set_pwck(pin_out,0);
	m_iobuf.set_pwck(pin_rst,pin_rst);		// release reset
	m_iobuf.set_data(0,50);
	// first trial
	m_iobuf.set_rdpt(9);
	m_iobuf.set_data(dpacc_rb_write,8);
	m_iobuf.set_data(0,5);
	m_iobuf.set_data(0x7b0c06db,32);
	m_iobuf.set_data(0,3);
	// second trial
	m_iobuf.set_rdpt(9);
	m_iobuf.set_data(dpacc_rb_write,8);
	m_iobuf.set_data(0,5);
	m_iobuf.set_data(0x7b0c06db,32);
	m_iobuf.set_data(0,3);
	// write key register address
	m_iobuf.set_rdpt(9);
	m_iobuf.set_data(apacc_addr_write,8);
	m_iobuf.set_data(0,5);
	m_iobuf.set_data(0x00050210,32);
	m_iobuf.set_data(0,3);
	// write key register address
	m_iobuf.set_rdpt(9);
	m_iobuf.set_data(apacc_addr_write,8);
	m_iobuf.set_data(0,5);
	m_iobuf.set_data(0x00050210,32);
	m_iobuf.set_data(0,3);
	// write acquire key
	m_iobuf.set_rdpt(9);
	m_iobuf.set_data(apacc_data_write,8);
	m_iobuf.set_data(0,5);
	m_iobuf.set_data(0xea7e30a9,32);
	m_iobuf.set_data(0,3);
	
	m_iobuf.set_data(0,100);
	return;
}

void d2xxProgrammer::add_jtagtoswd(void)
{
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	//	m_iobuf.set_data_msbf(0b0111100111100111, 16);
	m_iobuf.set_data_msbf(0x000079e7, 16);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0xffffffff, 32);
	m_iobuf.set_data(0, 50);
	m_iobuf.set_data(dpacc_id_read, 8);
	m_iobuf.set_data(0,41);
	return;
}

void d2xxProgrammer::add_commandissp(int cmd, unsigned char addr, unsigned char data)
{
	switch(cmd)
	{
	  case issp_data_read:
		m_iobuf.set_rdpt(14);
		m_iobuf.set_data_msbf(0x5,3);
		m_iobuf.set_data_msbf(addr,8);
		m_iobuf.set_data_msbf(0,10);
		m_iobuf.set_data_msbf(0xff,1);
		break;
	  case issp_data_write:
		m_iobuf.set_data_msbf(0x4,3);
		m_iobuf.set_data_msbf(addr,8);
		m_iobuf.set_data_msbf(data,8);
		m_iobuf.set_data_msbf(0xff,3);
		break;
	  case issp_cmd_write:
		m_iobuf.set_data_msbf(0x6,3);
		m_iobuf.set_data_msbf(addr,8);
		m_iobuf.set_data_msbf(data,8);
		m_iobuf.set_data_msbf(0xff,3);
		break;
	  case issp_cmd_null:
		m_iobuf.set_data_msbf(0,22);
		break;
	  default:
		m_iobuf.set_data_msbf(0, 50);
		break;
	}
	return;
}

void d2xxProgrammer::add_acquireissp(void)
{
	m_iobuf.set_data_msbf(0x6,3);
	m_iobuf.set_data_msbf(0,8);
	m_iobuf.set_data_msbf(0,8);
	m_iobuf.set_data_msbf(0,3);
	m_iobuf.set_data_msbf(0,22);
	m_iobuf.set_data_msbf(0,22);
	m_iobuf.set_data_msbf(0,22);
	m_iobuf.set_data_msbf(0,22);
	m_iobuf.set_data_msbf(0,22);
	return;
}

void d2xxProgrammer::execute_queue(void)
{
	int count = m_iobuf.count();
	if(count){
#define MAX_USB_TXRX_SIZE (1024*62)
		unsigned char *p = m_iobuf.bufferPointer();
		for(int i=0;i<count;){
			int txrxsz = count - i;
			if(txrxsz > MAX_USB_TXRX_SIZE)
				txrxsz = MAX_USB_TXRX_SIZE;
			send_and_recv(p+i,txrxsz);
			i += txrxsz;
		}
	}
	m_iobuf.purge();
}

bool d2xxProgrammer::beginCommand(void)
{
	m_queuingMode++;
	return true;
}

void d2xxProgrammer::endCommand(void)
{
	if(m_queuingMode){
		m_queuingMode--;
		if(!m_queuingMode){
			execute_queue();
		}
	}
}

int d2xxProgrammer::readWord(unsigned int *rbuf,int count)
{
	int i;
	for(i=0;i<count;i++){
		*rbuf++ = m_iobuf.get_data(32);
	}
	return i;
}

/*
unsigned int d2xxProgrammer::readOneWord(void)
{
	unsigned int rbuf;
	readWord(&rbuf, 1);
	return(rbuf);
}
*/
int d2xxProgrammer::readByte(unsigned char *rbuf,int count)
{
	int i;
	for(i=0;i<count;i++){
		*rbuf++ = m_iobuf.get_data(8);
	}
	return i;
}
/*
unsigned char d2xxProgrammer::readOneByte(void)
{
	unsigned char rbuf;
	readByte(&rbuf, 1);
	return(rbuf);
}
*/
/*
int d2xxProgrammer::readVISI(unsigned long *rbuf,int count)
{
	int i;
	for(i=0;i<count;i++){
		*rbuf++ = m_iobuf.get_data(16);
	}
	return i;
}
*/
int d2xxProgrammer::readAck(unsigned char *rbuf)
{
	*rbuf = m_iobuf.get_data(3);
	return 3;
}

int d2xxProgrammer::get_parity(unsigned long data)
{
	int i, parity = 0;
	for(i = 0; i < 32; i++)
	{
		parity ^= (data & 0x00000001L);
		data>>=1;
	}
	return(parity);
}
