#ifndef D2XXPROGRAMMER_H
#define D2XXPROGRAMMER_H

#include "PICProgrammer.h"

#include "config.h"
#if HAVE_WINDOWS_H
#  include <windows.h>
#  define usleep(x) Sleep(x/1000)
#endif
#ifdef _WIN32
#include "../ftd2xx_win.h"
#else
#include <ftd2xx.h>
#endif
#include "d2xxIOBuf.h"

class d2xxProgrammer : public PICProgrammer
{
	FT_HANDLE	m_ftHandle;
	d2xxIOBuf	m_iobuf;
	int			m_queuingMode;
	
	enum {
		pin_out = 4/*1*/,
		pin_in	= 8/*2*/,
		pin_clk = 16/*4*/,
		pin_vdd = 64/*8*/,
		pin_vpp = 128/*16*/,
		pin_rst = 32,
	};
	
public:
	d2xxProgrammer();
	~d2xxProgrammer();
	
	bool open(const char *port);
	void close();
	
	void powerOn(void);
	void powerOff(void);
	void reset(unsigned char release, unsigned char polarlity);
	
	void delay(unsigned long ns);
	
//	unsigned short command(int cmd,unsigned short data=0);
//	unsigned char command18(int cmd,unsigned short data=0);
//	unsigned short command30(int cmd,unsigned long data=0);
//	unsigned char command24(int cmd,unsigned char addr=0,unsigned char data=0);
	unsigned int commandswd(int cmd,unsigned int data=0);
	unsigned int switchjtagtoswd(void);
	unsigned int acquireswd(void);
	unsigned char commandissp(int cmd, unsigned char addr=0, unsigned char data=0);
	unsigned char acquireissp(void);
	
	bool beginCommand(void);
	void endCommand(void);
	
	int readWord(unsigned int *rbuf,int count);
//	unsigned int readOneWord(void);
	int readByte(unsigned char *rbuf,int count);
//	unsigned char readOneByte(void);
//	int readVISI(unsigned long *rbuf,int count);
	int readAck(unsigned char *rbuf);
	
private:
	void init_port(void);
	void power_on(int fst,int snd);
	void power_off(int fst,int snd);
	int send_and_recv(unsigned char *rwbuf,int size);
//	void add_command(int cmd,unsigned short data=0);
//	void add_command18(int cmd18,unsigned short data=0);
//	void add_command30(int cmd,unsigned long data=0);
//	void add_command24(int cmd,unsigned char addr=0,unsigned char data=0);
	void add_commandswd(int cmd,unsigned int data=0);
	void add_acquireswd(void);
	void add_jtagtoswd(void);
	void add_commandissp(int cmd, unsigned char addr=0, unsigned char data=0);
	void add_acquireissp(void);
	void execute_queue(void);
	int get_parity(unsigned long data);
};

#endif
