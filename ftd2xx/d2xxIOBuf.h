#ifndef D2XXIOBUF_H
#define D2XXIOBUF_H

class d2xxIOBuf
{
	unsigned char	m_ioLatch;
	unsigned char	m_pin_in;
	unsigned char	m_pin_out;
	unsigned char	m_pin_clk;
	unsigned char* 	m_txRxBuffer;
	unsigned long   m_txRxBufferSize;
	int				m_txRxCount;
	int				m_readCount;
	int				m_numberOfReadOffsets;
	int*			m_readOffsets;
	bool			m_clrReadOffsets;
public:
	d2xxIOBuf(unsigned char pin_in,unsigned char pin_out,unsigned char pin_clk)
		: m_ioLatch(0)
		, m_pin_in(pin_in)
		, m_pin_out(pin_out)
		, m_pin_clk(pin_clk)
		, m_txRxBuffer(0)
		, m_txRxBufferSize(126976)
		, m_txRxCount(0)
		, m_readCount(0)
		, m_numberOfReadOffsets(0)
		, m_readOffsets(0)
		, m_clrReadOffsets(0) {
		m_txRxBuffer = new unsigned char[m_txRxBufferSize];
		m_readOffsets = new int[m_txRxBufferSize>>5];
	}
	~d2xxIOBuf(){
		delete [] m_txRxBuffer;
		delete [] m_readOffsets;
	}
	void set_port(unsigned char port,unsigned char data){
		m_ioLatch &= ~port;
		m_ioLatch |= (port & data);
		set_stbl(1);
	}
	unsigned char* bufferPointer(void){
		return m_txRxBuffer;
	}
	int count(void){
		return m_txRxCount;
	}
	void set_data(unsigned long data,int length){
		unsigned long bit = 1;
		for(int i=0; i<length; i++){
			unsigned char latch = m_ioLatch;
			if(data&bit)
				latch |= m_pin_out;
			m_txRxBuffer[m_txRxCount++] = latch; /* clk L */
			m_txRxBuffer[m_txRxCount++] = latch | m_pin_clk; /* clk H */
			bit <<= 1;
		}
	}
	void set_data_msbf(unsigned long data, int length){
		unsigned long bit = 1;
		bit <<= (length - 1);
		for(int i=0; i<length; i++){
			unsigned char latch = m_ioLatch;
			if(data&bit)
				latch |= m_pin_out;
			m_txRxBuffer[m_txRxCount++] = latch; /* clk L */
			m_txRxBuffer[m_txRxCount++] = latch | m_pin_clk; /* clk H */
			bit >>= 1;
		}
	}
	void set_stbl(int count){
		for(int i=0; i<count; i++){
			m_txRxBuffer[m_txRxCount++] = m_ioLatch;
			m_txRxBuffer[m_txRxCount++] = m_ioLatch;
		}
	}
	void set_rdpt(int offset = 0){
		if(m_clrReadOffsets){
			m_clrReadOffsets = false;
			m_numberOfReadOffsets = 0;
		}
		m_readOffsets[m_numberOfReadOffsets++] = m_txRxCount + (offset * 2) + 1;/* read clk L position */
	}
	void set_pwck(unsigned char port, unsigned char data){
		m_ioLatch &= ~port;
		m_ioLatch |= (port & data);
		m_txRxBuffer[m_txRxCount++] = m_ioLatch;
		m_txRxBuffer[m_txRxCount++] = m_ioLatch | m_pin_clk;
	}
	unsigned long get_data(int length){
		if(m_readCount < m_numberOfReadOffsets){
			unsigned long bit = 1;
			unsigned long ret = 0;
			unsigned char *p = m_txRxBuffer + m_readOffsets[m_readCount++];
			for(int i=0; i<length; i++){
				if(*p & m_pin_in)
					ret |= bit;
				p += 2;
				bit <<= 1;
			}
			return ret;
		}
		return 0;
	}
	unsigned long get_data_msbf(int length){
		if(m_readCount < m_numberOfReadOffsets){
			unsigned long bit = 1;
			unsigned long ret = 0;
			unsigned char *p = m_txRxBuffer + m_readOffsets[m_readCount++];
			bit <<= (length - 1);
			for(int i=0; i<length; i++){
				if(*p & m_pin_in)
					ret |= bit;
				p += 2;
				bit >>= 1;
			}
			return ret;
		}
		return 0;
	}
	void purge(void){
		m_txRxCount = m_readCount = 0;
		m_clrReadOffsets = true;
	}
};

#endif
