#ifndef D2XXPROGRAMMERFACTORY_H
#define D2XXPROGRAMMERFACTORY_H

class PICProgrammer;
class d2xxProgrammerFactory
{
	d2xxProgrammerFactory(){}
public:
	~d2xxProgrammerFactory(){}
	static PICProgrammer* create(void);
};

#endif
