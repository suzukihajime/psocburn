#include "config.h"
#include "d2xxProgrammerFactory.h"
#if BUILD_FTD2XX==1
#include "d2xxProgrammer.h"
#endif

PICProgrammer* d2xxProgrammerFactory::create(void)
{
#if BUILD_FTD2XX==1
	return new d2xxProgrammer();
#else
	return 0;
#endif
}
