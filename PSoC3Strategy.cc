// PSoC3Strategy.cc
//    2011.12 aasoukai128

#include "PICDevice.h"
#include "PSoC3Strategy.h"
#include "progress.h"

PSoC3Strategy::PSoC3Strategy(PICDevice &dev,PICProgrammer &prog)
	: PICStrategy(dev)
	, m_memory(dev)
	, m_piccmd(prog)
{
}

PSoC3Strategy::~PSoC3Strategy()
{
}

bool PSoC3Strategy::reset(void)
{
	m_piccmd.exitDebugMode();
	return(m_piccmd.enterDebugMode());
}

bool PSoC3Strategy::final()
{
	m_piccmd.exitDebugMode();
	return true;
}

bool PSoC3Strategy::deviceCheck()
{
	if(!reset())
	{
		fprintf(stderr, "[ERROR]: device couldn't be acquired\n");
		return false;
	}
	if (m_device.GetDeviceID()){
		unsigned int devid;
		m_piccmd.readDataFromDpacc(&devid);
		if(m_device.GetDeviceID() != devid) {
			fprintf(stderr, "[ERROR]: device not match\n");
			return false;
		}
	}
	return true;
}


bool PSoC3Strategy::chipErase()
{
	progress progress1;
	bool ret;
	
	progress1.begin("Chip Erase...");
	/* erase start */
	ret = !m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);	// First initiation key
	ret |= !m_piccmd.loadDataForApacc(0x00004720, 0x000000dc);	// Second key:00DC(0xD3 + 0x09); 0x09 is Erase All opcode
	ret |= !m_piccmd.loadDataForApacc(0x00004720, 0x00000009);	// erase all opcode
	ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	progress1.end("Done");
	if(!ret) {
		return true;
	} else {
		fprintf(stderr, "[ERROR]: erase failed\n");
		return false;
	}
}


bool PSoC3Strategy::writeMemory(unsigned char *buf, unsigned char *conf, int rowCount, bool isEccEnable, int temp)
{
	int count, data, blockSize, rowLength, ret = 0;
	
//	printf("start\n");
	blockSize = m_device.GetBlockSize();
	if(isEccEnable) {
		rowLength = m_device.GetBlockSize();
	} else {
		rowLength = m_device.GetBlockSize() + m_device.GetEccBlockSize();
	}
	// write even
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x00000000, 0x0002d5b6);
	for(count = 0; count < rowLength; count += 4)
	{
		if(count < blockSize) {
			data =    (buf[count + 0])
					| (buf[count + 1]<<8)
					| (buf[count + 2]<<16)
					| (buf[count + 3]<<24);
		} else {
			data =    (conf[count - blockSize + 0])
					| (conf[count - blockSize + 1]<<8)
					| (conf[count - blockSize + 2]<<16)
					| (conf[count - blockSize + 3]<<24);
		}
		m_piccmd.loadDataForApacc(0x00000004 + count, data);
	}
	m_piccmd.loadDataForApacc(rowLength + 0x04, 0xb6000000);
	m_piccmd.loadDataForApacc(rowLength + 0x08, 0x000007da);
	if(m_device.GetSiliconRev() >= 3) {
		m_piccmd.loadDataForApacc(rowLength + 0x0c, (temp & 0x00ffff00) | (rowCount & 0x000000ff));
	}
	m_piccmd.loadDataForApacc(0x00007018, 0x00000000);
	m_piccmd.loadDataForApacc(0x00007010, 0x00000021);
	m_piccmd.loadDataForApacc(0x00007600, 0x00000080);
	m_piccmd.loadDataForApacc(0x00007604, 0x00000000);
	if(m_device.GetSiliconRev() >= 3) {
		m_piccmd.loadDataForApacc(0x00007800, 0x01ff0000 + rowLength + 15);
	}
	m_piccmd.loadDataForApacc(0x00007804, 0x47200000);
	m_piccmd.endCommand();
	// wait until SPC has done previous request
	ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	m_piccmd.loadDataForApacc(0x00007014, 0x00000001);
	
	// write odd
	rowCount++;
	buf += m_device.GetBlockSize();
	conf += m_device.GetEccBlockSize();
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x00000200, 0x0002d5b6);
	for(count = 0; count < rowLength; count += 4)
	{
		if(count < blockSize) {
			data =    (buf[count + 0])
					| (buf[count + 1]<<8)
					| (buf[count + 2]<<16)
					| (buf[count + 3]<<24);
		} else {
			data =    (conf[count - blockSize + 0])
					| (conf[count - blockSize + 1]<<8)
					| (conf[count - blockSize + 2]<<16)
					| (conf[count - blockSize + 3]<<24);
		}
		m_piccmd.loadDataForApacc(0x00000204 + count, data);
	}
	m_piccmd.loadDataForApacc(rowLength + 0x204, 0xb6000000);
	m_piccmd.loadDataForApacc(rowLength + 0x208, 0x000007da);
	if(m_device.GetSiliconRev() >= 3) {
		m_piccmd.loadDataForApacc(rowLength + 0x20c, (temp & 0x00ffff00) | (rowCount & 0x000000ff));
	}
	m_piccmd.loadDataForApacc(0x00007028, 0x00000100);
	m_piccmd.loadDataForApacc(0x00007020, 0x00000021);
	m_piccmd.loadDataForApacc(0x00007608, 0x00000080);
	m_piccmd.loadDataForApacc(0x0000760c, 0x00000000);
	if(m_device.GetSiliconRev() >= 3) {
		m_piccmd.loadDataForApacc(0x00007808, 0x01ff0000 + rowLength + 15);
	}
	m_piccmd.loadDataForApacc(0x0000780c, 0x47200200);
	m_piccmd.endCommand();
	// wait until SPC has done previous request
	ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	m_piccmd.loadDataForApacc(0x00007024, 0x00000001);
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

bool PSoC3Strategy::verifyMemory(unsigned char *buf, unsigned char *conf, int rowCount, bool isEccEnable)
{
	int address, ret = 0;
	unsigned char *rbuf, *rconf;
	rbuf = new unsigned char[m_device.GetBlockSize()];
	rconf = new unsigned char[m_device.GetEccBlockSize()];
	
	address = rowCount * m_device.GetBlockSize();
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
	m_piccmd.loadDataForApacc(0x00004720, 0x000000d7);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000004);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x00004720, (address>>16) & 0x000000ff);
	m_piccmd.loadDataForApacc(0x00004720, (address>>8)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x00004720, (address>>0)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x00004720, 0x000000ff);
	m_piccmd.endCommand();
	ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
	/* read multiple bytes */
	ret |= !m_piccmd.readMultipleBytesFromApacc(0x00004720, rbuf, m_device.GetBlockSize());
	for(int i = 0; i < m_device.GetBlockSize(); i++) {
		ret |= (rbuf[i] != buf[i]);
	}
	
	if(!isEccEnable) {
		address = rowCount * m_device.GetEccBlockSize() | 0x00800000;
		m_piccmd.beginCommand();
		m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);		// first initiation key
		m_piccmd.loadDataForApacc(0x00004720, 0x000000d7);		// 0xd2 + READ_MULTIPLE_BYTE opcode
		m_piccmd.loadDataForApacc(0x00004720, 0x00000004);		// READ_MULTIPLE_BYTE opcode
		m_piccmd.loadDataForApacc(0x00004720, 0x00000000);		// Arrar ID
		m_piccmd.loadDataForApacc(0x00004720, (address>>16) & 0x000000ff);
		m_piccmd.loadDataForApacc(0x00004720, (address>>8)  & 0x000000ff);
		m_piccmd.loadDataForApacc(0x00004720, (address>>0)  & 0x000000ff);
		m_piccmd.loadDataForApacc(0x00004720, 0x0000001f);
		m_piccmd.endCommand();
		ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
		ret |= !m_piccmd.readMultipleBytesFromApacc(0x00004720, rconf, m_device.GetEccBlockSize());
		for(int i = 0; i < m_device.GetEccBlockSize(); i++) {
			ret |= (rconf[i] != conf[i]);
		}
	}
	delete [] rbuf;
	delete [] rconf;
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

bool PSoC3Strategy::readMemory(unsigned char *buf, unsigned char *conf, int rowCount, bool isEccEnable)
{
	int rowLength, address, ret = 0;
	
	if(isEccEnable) {
		rowLength = m_device.GetBlockSize();
	} else {
		rowLength = m_device.GetBlockSize() + m_device.GetEccBlockSize();
	}
	address = rowCount * m_device.GetBlockSize();
	
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
	m_piccmd.loadDataForApacc(0x00004720, 0x000000d7);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000004);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x00004720, (address>>16) & 0x000000ff);
	m_piccmd.loadDataForApacc(0x00004720, (address>>8)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x00004720, (address>>0)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x00004720, 0x000000ff);
	m_piccmd.endCommand();
	ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
	/* read multiple bytes */
	ret |= !m_piccmd.readMultipleBytesFromApacc(0x00004720, buf, m_device.GetBlockSize());
	
	if(!isEccEnable) {
		address = rowCount * m_device.GetEccBlockSize() | 0x00800000;
		m_piccmd.beginCommand();
		m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);		// first initiation key
		m_piccmd.loadDataForApacc(0x00004720, 0x000000d7);		// 0xd2 + READ_MULTIPLE_BYTE opcode
		m_piccmd.loadDataForApacc(0x00004720, 0x00000004);		// READ_MULTIPLE_BYTE opcode
		m_piccmd.loadDataForApacc(0x00004720, 0x00000000);		// Arrar ID
		m_piccmd.loadDataForApacc(0x00004720, (address>>16) & 0x000000ff);
		m_piccmd.loadDataForApacc(0x00004720, (address>>8)  & 0x000000ff);
		m_piccmd.loadDataForApacc(0x00004720, (address>>0)  & 0x000000ff);
		m_piccmd.loadDataForApacc(0x00004720, 0x0000001f);
		m_piccmd.endCommand();
		ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
		ret |= !m_piccmd.readMultipleBytesFromApacc(0x00004720, conf, m_device.GetEccBlockSize());
	}
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

unsigned int PSoC3Strategy::checkTemperature()
{
	unsigned int temp;
	unsigned char rbuf[2];
	m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
	m_piccmd.loadDataForApacc(0x00004720, 0x000000e1);
	m_piccmd.loadDataForApacc(0x00004720, 0x0000000e);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000003);
	m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
	m_piccmd.readMultipleBytesFromApacc(0x00004720, rbuf, 3);
	m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	temp = (((int)rbuf[1]<<8) & 0x0000ff00) | (((int)rbuf[2]<<16) & 0x00ff0000);
	return(temp);
}

bool PSoC3Strategy::writeNVLatch(unsigned char *buf, int size)
{
	unsigned int *rbuf = new unsigned int[m_device.GetNvlSize()];
	int rewrite = 0;
	bool ret;
	
//	fprintf(stderr, "Checking NVL...\n");
	// read nvl from device
	ret = !m_piccmd.loadDataForApacc(0x00005112, 0x00000000);
	m_piccmd.loadDataForApacc(0x00005113, 0x00000004);
	m_piccmd.loadDataForApacc(0x00005114, 0x00000000);
	m_piccmd.loadDataForApacc(0x00005110, 0x00000004);
	for(int i = 0; i < m_device.GetNvlSize(); i++)
	{
		m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
		m_piccmd.loadDataForApacc(0x00004720, 0x000000d6);
		m_piccmd.loadDataForApacc(0x00004720, 0x00000003);
		m_piccmd.loadDataForApacc(0x00004720, 0x00000080);
		m_piccmd.loadDataForApacc(0x00004720, i);
		ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
		m_piccmd.readDataFromApacc(0x00004720, &rbuf[i]);
		ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	}
	
	// check if rewrite is needed
	for(int i = 0; i < m_device.GetNvlSize(); i++) {
		rewrite |= (buf[i] == (unsigned char)rbuf[i]) ? 0 : 1;
	}
	
	// rewrite nvl memory
	if(rewrite)
	{
		progress progress1;
		progress1.begin("Rewrite NVL");
		progress1.setmax(m_device.GetNvlSize());
		for(int i = 0; i < m_device.GetNvlSize(); i++)
		{
			m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
			m_piccmd.loadDataForApacc(0x00004720, 0x000000d3);
			m_piccmd.loadDataForApacc(0x00004720, 0x00000000);
			m_piccmd.loadDataForApacc(0x00004720, 0x00000080);
			m_piccmd.loadDataForApacc(0x00004720, i);
			m_piccmd.loadDataForApacc(0x00004720, buf[i]);
			ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
			progress1.setval(i);
		}
		
		m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
		m_piccmd.loadDataForApacc(0x00004720, 0x000000d9);
		m_piccmd.loadDataForApacc(0x00004720, 0x00000006);
		m_piccmd.loadDataForApacc(0x00004720, 0x00000080);
		ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
		progress1.end("Done");
		// check if ecc Enable is changed
	}
	else
	{
		fprintf(stderr, "No need to rewrite NVL\n");
	}
	delete [] rbuf;
	return(ret ? false : true);
}

unsigned int PSoC3Strategy::readChecksum(void)
{
	unsigned int checksum;
	int block, prog;
	unsigned char rbuf[4];
	
	block = m_device.GetBlockSize();
	prog = m_device.GetProgSize();
	m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
	m_piccmd.loadDataForApacc(0x00004720, 0x000000df);
	m_piccmd.loadDataForApacc(0x00004720, 0x0000000c);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x00004720, prog/block - 1);
	m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
	m_piccmd.readMultipleBytesFromApacc(0x00004720, rbuf, 4);
	m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	checksum =    (unsigned int)rbuf[2]<<8
				| (unsigned int)rbuf[3];
	return checksum;
}


bool PSoC3Strategy::readBuffer()
{
	progress progress1;
	int size, block;
	bool isEccEnable;
	
	size = m_device.GetProgSize();
	block = m_device.GetBlockSize();
	isEccEnable = (m_memory[0x90000003] & 0x00000008) ? true : false;
	progress1.begin("Program Area Reading");
	progress1.setmax(size);
	//progress1.end("Done");
	for(int addr = 0; addr < size; addr += block) {
		if(!readMemory(&m_memory[addr], &m_memory[0x80000000 + addr/8], addr/block, isEccEnable)) { 		// magic number '8' fixme
			fprintf(stderr, "[ERROR] Reading memory failed - %d\n", addr/block);
			return false;
		}
		progress1.setval(addr);
	}
	progress1.end("Done");
	
	return true;
}

bool PSoC3Strategy::writeBuffer()
{
	progress progress1;
	int size, block, temp;
	bool isEccEnable;
	
	// first write none volatile latch
	size = m_device.GetNvlSize();
	writeNVLatch(&m_memory[0x90000000], size);
	
	// then write program flash memory
	// first Get temperature twice
	if(m_device.GetSiliconRev() >= 3) {		// Silicon ES3 or greater only
		checkTemperature();
		temp = checkTemperature();
	}
	// check length of row
	isEccEnable = (m_memory[0x90000003] & 0x00000008) ? true : false;
	// check program memory size and program block size
	size = m_device.GetProgSize();
	block = m_device.GetBlockSize();
	progress1.begin("Program Area Writing");
	progress1.setmax(size);
	m_piccmd.setDMAAccessWidth(4);
	for(int addr = 0; addr < size; addr += block*2){
		if(!writeMemory(&m_memory[addr], &m_memory[0x80000000 + addr/8], addr/block, isEccEnable, temp)) {		// magic number '8' fix me
			fprintf(stderr, "[ERROR] Program memory failed\n");
			return false;
		}
		progress1.setval(addr);
	}
	m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	m_piccmd.setDMAAccessWidth(1);
	int ch;
	if((ch = readChecksum()) != (((int)m_memory[0x90300000]<<8) | m_memory[0x90300001])) {
		progress1.end("Failed (Checksum not match)");
		printf("%x, %x", ch, ((int)m_memory[0x90300000]<<8) | m_memory[0x90300001]);
	} else {
		progress1.end("Done");
	}
	return true;
}

bool PSoC3Strategy::verifyBuffer()
{
	progress progress1;
	int size, block;
	bool isEccEnable;
	
	size = m_device.GetProgSize();
	block = m_device.GetBlockSize();
	isEccEnable = (m_memory[0x90000003] & 0x00000008) ? true : false;
	progress1.begin("Program Area Verifing");
	progress1.setmax(size);
	
	for(int addr = 0; addr < size; addr += block) {
		if(!verifyMemory(&m_memory[addr], &m_memory[0x80000000 + addr/8], addr/block, isEccEnable)) {		// magic number '8' fixme
			fprintf(stderr, "[ERROR] Verify failed\n");
			return false;
		}
		progress1.setval(addr);
	}
	progress1.end("Done");
	
	return true;
}
