// utils.cc
//    2007.6 Y.Senta

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "utils.h"

int dbprintf(int l, const char *f, ...){
  int ret=0;
  
  if (l<g_nDebugLevel){
	fprintf(stderr, "[%d]: ", l);
	va_list arg;
	va_start(arg, f);
	ret=vfprintf(stderr, f, arg);
	va_end(arg);
  }
  
  return ret;
}

char *setstring(const char *c){
  char *ret=new char[strlen(c)+1];
  strcpy(ret,c);
  return ret;
}

char *chopstr(char *c){
  char *p=c;
  
  while (*p==' ' || *p=='\t')
	p++;
  
  char *q=p;
  while (!(*q==' ' || *q=='\t' || *q=='\r' || *q=='\n' || *q==0))
	q++;
  *q=0;
  
  return p;
}

bool gethexbyte(int *r, const char *c, int n){
  *r=0;
  for (int i=0;i<n*2;i++){
    (*r) *= 0x10;
    if (c[i]>='0' && c[i]<='9')
      (*r) += c[i]-'0';
    else if (c[i]>='a' && c[i]<='f')
      (*r) += c[i]-'a'+0x0a;
    else if (c[i]>='A' && c[i]<='F')
      (*r) += c[i]-'A'+0x0a;
    else
      return false;
  }
  return true;
}

char *cfgstrcmp(char *s1,const char *s2){
	if (s1 && s2){
		while(*s1==' ' || *s1=='\t') s1++;
		if (strncmp(s1, s2, strlen(s2)) == 0){
			return chopstr(s1+strlen(s2));
		}
	}
	return 0;
}
