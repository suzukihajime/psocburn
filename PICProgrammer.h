#ifndef PICPROGRAMMER_H
#define PICPROGRAMMER_H


class PICProgrammer
{
public:
	enum commands {
		load_config = 0x0, load_prog = 0x2, load_data = 0x3, read_prog = 0x4,
		read_data = 0x5, inc_addr = 0x6, begin_prog = 0x8, begin_prog_only = 0x18,
		end_prog_a = 0xa, end_prog_e = 0xe, end_prog_17 = 0x17,	erase_prog = 0x9,
		erase_data = 0xb, row_erase_prog = 0x11, chip_erase = 0x1f,
	};
	
	enum commands18 {
		shift_in = 0x0, shift_out = 0x2, tread = 0x8, tread_inc = 0x9, tread_dec = 0xa,
		inc_tread = 0xb, twrite = 0xc, twrite_inc = 0xd, twrite_dec = 0xe, twrite_start = 0xf,
		nop_prog = 0x10, nop_erase = 0x12, end_prog_erase = 0x13,
	};
	
	enum commands30 {
		six = 0x0, regout = 0x1,
	};
	
	enum commands24 {
	};
	
	enum commandsswd {
		dpacc_id_read = 0xa5,
		apacc_data_read = 0x9f,
		apacc_addr_write = 0x8b,
		apacc_data_write =0xbb,
		apacc_ctrlstat_write = 0xa3,
		dpacc_rb_write = 0x99,
		dpacc_config_write = 0xa9,		// the same code as dpacc_ctrlstat_write
		dpacc_select_write = 0xb1,
		swd_command_reset = 0,
	};
	
	enum commandsissp {
		issp_cmd_null = 0,
		issp_cmd_write = 0x01,
		issp_data_write =0x02,
		issp_data_read =0x03,
	};
	
	enum PowerSequence {
		VddBeforeVpp = 0,
		VppBeforeVdd = 1,
	};
	
	enum ResetRelease {
		NormalRelease = 0,
		DebugModeRelease = 1,
	};
	
	enum ResetPolarlity {
		PositiveReset = 0,
		NegativeReset = 1,
	};
	
	virtual bool open(const char *port) = 0;
	virtual void close() = 0;
	
	virtual void powerOn(void) = 0;
	virtual void powerOff(void) = 0;
	virtual void reset(unsigned char release, unsigned char polarity=1) = 0;
	
	virtual void delay(unsigned long ns) = 0;
	
//	virtual unsigned short command(int cmd,unsigned short dat_n=0) = 0;
//	virtual unsigned char command18(int cmd,unsigned short data=0) = 0;
//	virtual unsigned short command30(int cmd,unsigned long data=0) = 0;
//	virtual unsigned char command24(int cmd,unsigned char addr=0,unsigned char data=0) = 0;
	virtual unsigned int commandswd(int cmd,unsigned int data=0) = 0;
	virtual unsigned int acquireswd(void) = 0;
	virtual unsigned int switchjtagtoswd(void) = 0;
	virtual unsigned char commandissp(int cmd,unsigned char addr=0,unsigned char data=0) = 0;
	virtual unsigned char acquireissp(void) = 0;
	
	virtual bool beginCommand(void){
		return false;
	}
	virtual void endCommand(void){}
	virtual int readWord(unsigned int *rbuf,int count){
		return 0;
	}
	virtual unsigned int readOneWord(void){
		unsigned int w;
		readWord(&w,1);
		return w;
	}
	virtual int readByte(unsigned char *rbuf,int count){
		return 0;
	}
	virtual unsigned char readOneByte(void){
		unsigned char w;
		readByte(&w,1);
		return w;
	}
/*	virtual int readVISI(unsigned long *rbuf,int count){
		return 0;
	}*/
/*	virtual unsigned long readOneVISI(void){
		unsigned long w;
		readVISI(&w,1);
		return w;
	}*/
	
	virtual ~PICProgrammer(){}
};

#endif
