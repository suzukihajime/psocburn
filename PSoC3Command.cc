// PSoC3Command.cc
//    2011.12 aasoukai128


#include "PSoC3Command.h"

PSoC3Command::PSoC3Command(PICProgrammer &prog)
  : m_prog(prog)
{
}

PSoC3Command::~PSoC3Command()
{
}

bool PSoC3Command::loadDataForApacc(unsigned int addr, unsigned int data)
{
	unsigned int ret = 0;
	m_prog.commandswd(PICProgrammer::apacc_addr_write, addr);
	ret |= !m_prog.commandswd(PICProgrammer::apacc_data_write, data);
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

bool PSoC3Command::pollDataInApacc(unsigned int addr, unsigned int data)
{
	int cnt = 0;
	m_prog.commandswd(PICProgrammer::apacc_addr_write, addr);
	m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	cnt = 1000;
	do{
		if((m_prog.commandswd(PICProgrammer::apacc_data_read, 0) & 0x000000ff) == data){
			return true;
		}/* wait for a second */
	} while(--cnt > 0);
	return false;
}

bool PSoC3Command::setDMAAccessWidth(int width)
{
	switch(width){
	  case 1:
		return(m_prog.commandswd(PICProgrammer::dpacc_config_write, 0x00000000));
	  case 2:
		return(m_prog.commandswd(PICProgrammer::dpacc_config_write, 0x00000002));
	  case 4:
		return(m_prog.commandswd(PICProgrammer::dpacc_config_write, 0x00000004));
	  default:
		return(false);
	}
}

bool PSoC3Command::readDataFromApacc(unsigned int addr, unsigned int *rbuf)
{
	bool ret = 0;
	ret = !m_prog.commandswd(PICProgrammer::apacc_addr_write, addr);
	m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	*rbuf = m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

bool PSoC3Command::readMultipleBytesFromApacc(unsigned int addr, unsigned char *rbuf, int length)
{	/* for PSoC3 ES3 support */
	unsigned int ret = 0, count;
	unsigned int *tempbuf = new unsigned int[length];
	unsigned int *ptr;
	
	ret |= !m_prog.commandswd(PICProgrammer::apacc_addr_write, addr);
	ret |= !m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	m_prog.beginCommand();
	count = length;
	while(count--) {
		m_prog.commandswd(PICProgrammer::apacc_data_read, 0);
	}
	m_prog.endCommand();
	m_prog.readWord(tempbuf, length);
	ptr = tempbuf;
	while(length--) {
		*rbuf++ = (unsigned char)*ptr++;
	}
	delete [] tempbuf;
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

void PSoC3Command::readDataFromDpacc(unsigned int *rbuf)
{
	*rbuf = m_prog.commandswd(PICProgrammer::dpacc_id_read, 0);
	return;
}

void PSoC3Command::beginCommand(void)
{
	m_prog.beginCommand();
	return;
}

void PSoC3Command::endCommand(void)
{
	m_prog.endCommand();
	return;
}

/*
int PSoC3Command::wordWriteCycle(int cmd,int begin,int end,int wai,const unsigned char *wbuf,int count)
{
	return 0;
}

int PSoC3Command::wordReadCycle(int cmd,unsigned char *rbuf,int count)
{
	return 0;
}

void PSoC3Command::configureNonVolatileLatch(void)
{
}
*/
bool PSoC3Command::enterDebugMode(void)
{
	unsigned short cnt = 0;
	bool ret;
	
	m_prog.powerOn();
	m_prog.reset(PICProgrammer::NormalRelease, PICProgrammer::NegativeReset);
	cnt = 10;
	while(m_prog.acquireswd() && --cnt > 0) {}
	if(cnt == 0)
	{
//		fprintf(stderr, "device can't be acquired\n");
		return false;
	}
/*	m_prog.commandswd(PICProgrammer::dpacc_rb_write, 0x7b0c06db);
	if(!loadDataForApacc(0x00050210, 0xea7e30a9)) {
		return false;
	}*/
	
	// debug port acquire succeeded
	//m_prog.commandswd(PICProgrammer::dpacc_config_write, 0x00000000);
	ret = !setDMAAccessWidth(1);
	ret |= !loadDataForApacc(0x00050220, 0x000000b3);	// Halt CPU, Enable Debug-on-chip (DoC) access
	ret |= !loadDataForApacc(0x000046ea, 0x00000001);	// Halt CPU
	ret |= !loadDataForApacc(0x000043a0, 0x000000bf);	// Enable individual sub-system of chip
	ret |= !loadDataForApacc(0x00004200, 0x00000002);	// IMO set to 24 MHz
	if(!ret)
	{
		return true;		// entering debug mode succeeded
	} else {
		return false;
	}
}

void PSoC3Command::exitDebugMode(void)
{
	m_prog.reset(PICProgrammer::NormalRelease, PICProgrammer::NegativeReset);
	m_prog.powerOn();
	m_prog.delay(10*1000*1000);	// > 10ms
}
/*
void PSoC3Command::configureDevice(void)
{
	
}
*/