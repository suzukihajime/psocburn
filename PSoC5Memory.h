// PSoC5Memory.h
//    2012.8 aasoukai128

#ifndef PSoC5MEMORY_H
#define PSoC5MEMORY_H

#include "PICMemory.h"

class hexline;
class PSoC5Memory : public PICMemory
{
	unsigned char *m_pCodeMemory;
	unsigned char *m_pConfigMemory;
	unsigned char m_NVLatch[4];
	unsigned char m_Checksum[2];
	unsigned int m_JtagID;
	unsigned char m_SiliconRev;
	unsigned char m_DebugEnable;
	unsigned long m_codeSize;
	unsigned long m_configSize;
public:
	PSoC5Memory(PICDevice &device);
	~PSoC5Memory();
	bool readFromFile(FILE *fp);
	bool saveToFile(FILE *fp);
	void setTestData(void);
	unsigned char& operator[](unsigned long address);
	unsigned long codeSize(void){
		return m_codeSize;
	}
	unsigned long configSize(void){
		return m_configSize;
	}
	unsigned int getJtagID(void) {
		return m_JtagID;
	}
private:
	void store(unsigned long addr,unsigned char data);
	void writedata(hexline &hex,unsigned long addr,unsigned long count,unsigned char *data);
};

/*
#define MAX_CODE_SIZE	0x40000		// 256kBytes
#define	MAX_CONFIG_SIZE		0x8000		// 32kBytes

class hexline;

class PSoC5Memory : public PICMemory
{
	unsigned char *m_pCodeMemory;
	unsigned char *m_pConfigMemory;
	unsigned char m_NVLatch[4];
	unsigned short m_Checksum;
	unsigned int m_JtagID;
	unsigned char m_SiliconRev;
	unsigned char m_DebugEnable;
	
	void writeWords(hexline &hex,int begin,int end,unsigned short msk);
	
public:
	
	PSoC5Memory(PICDevice &device)
		: PICMemory(device) {
		m_pCodeMemory = new unsigned short[MAX_CODE_SIZE];
		for (int i=0;i<MAX_CODE_SIZE;i++)
			m_pCodeMemory[i]=0xff;
		m_pConfigMemory = new unsigned short[MAX_CONFIG_SIZE];
		for(int i = 0; i < MAX_CONFIG_SIZE; i+++)
			m_pConfigMemory[i] = 0xff;
		for(int i = 0; i < sizeof(m_NVLatch); i++)
			m_NVLatch[i] = 0xff;
		m_Checksum = 0;
		m_JtagID = 0xffffffff;
		m_SiliconRev = 0xff;
		m_DebugEnable = 0;
	}
	
	~PSoC5Memory(){
		if (m_pCodeMemory)
			delete [] m_pCodeMemory;
		if(m_pConfigMemory)
			delete [] m_pConfigMemory;
	}
	
	bool readFromFile(FILE *fp);
	bool saveToFile(FILE *fp);
	void setTestData(void);
	
	unsigned short& operator[](unsigned long address){
		return m_pCodeMemory[address];
	}
	unsigned short& operator[](unsigned long address){
		return m_pConfigMemory[address];
	}
};
*/
#endif
