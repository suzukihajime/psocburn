// PSoC3Memory.cc
//    2011.12 aasoukai128

#include <stdlib.h>
#include <string.h>
#include "PSoC3Memory.h"
#include "PICDevice.h"
#include "hexline.h"

PSoC3Memory::PSoC3Memory(PICDevice &device)
	: PICMemory(device)
	, m_codeSize(0)
	, m_configSize(0)
{
	m_pCodeMemory = new unsigned char[device.GetProgSize()];
	m_pConfigMemory = new unsigned char[device.GetConfSize()];
	if (m_pCodeMemory)
		memset(m_pCodeMemory,0xff,device.GetProgSize());
	if (m_pConfigMemory)
		memset(m_pConfigMemory,0xff,device.GetConfSize());
	memset(&m_JtagID,0,sizeof(m_JtagID));
	memset(m_NVLatch,0xff,sizeof(m_NVLatch));
	memset(m_Checksum,0xff,sizeof(m_Checksum));
}

PSoC3Memory::~PSoC3Memory()
{
	if(m_pCodeMemory)
		delete [] m_pCodeMemory;
	if(m_pConfigMemory)
		delete [] m_pConfigMemory;
}

void PSoC3Memory::store(unsigned long addr,unsigned char data)
{
	unsigned long offset;
	
	if(addr >= 0x90500000) {	// metadata
		offset = addr - 0x90500000;
		switch(offset++){
		  case 0:
		  case 1:	// hex file version
		  case 2:
			m_JtagID = (m_JtagID & 0x00ffffff) | ((unsigned int)data<<24);
			break;
		  case 3:
			m_JtagID = (m_JtagID & 0xff00ffff) | ((unsigned int)data<<16);
			break;
		  case 4:
			m_JtagID = (m_JtagID & 0xffff00ff) | ((unsigned int)data<<8);
			break;
		  case 5:
			m_JtagID = (m_JtagID & 0xffffff00) | ((unsigned int)data);
			break;
		  case 6:
			m_SiliconRev = data;
			break;
		  case 7:
			m_DebugEnable = data;
			break;
		  default:
			break;
		}
	}else if(addr >= 0x90400000) {	// flash protection data
		;
	}else if(addr >= 0x90300000){	// checksum
		offset = addr - 0x90300000;
		if(offset < sizeof(m_Checksum)){
			m_Checksum[offset++] = data;
		}
	}else if(addr >= 0x90100000){	// secured device mode configuration
		;	// do nothing
	}else if(addr >= 0x90000000){	// NVLatch
		offset = addr - 0x90000000;
		if(offset < sizeof(m_NVLatch)){
			m_NVLatch[offset++] = data;
		}
	}else if(addr >= 0x80000000){		// configuration data memory
		offset = addr - 0x80000000;
		if(offset < m_device.GetConfSize()){
			m_pConfigMemory[offset++] = data;
			
			if(offset > m_configSize){
				m_configSize = offset;
			}
		}
	}else{						// Code
		offset = addr;
		if(offset < m_device.GetProgSize()){
			m_pCodeMemory[offset++] = data;
			
			if(offset > m_codeSize){
				m_codeSize = offset;
			}
		}
	}
}

unsigned char& PSoC3Memory::operator[](unsigned long addr)
{
	unsigned long offset;
	if(addr >= 0x90300000){	// Configuration
		offset = addr - 0x90300000;
		return m_Checksum[offset % sizeof(m_Checksum)];
	} else if(addr >= 0x90000000){	// NVLatch
		offset = addr - 0x90000000;
		return m_NVLatch[offset % sizeof(m_NVLatch)];
	}else if(addr >= 0x80000000){		// configuration data memory
		offset = addr - 0x8000000;
		return m_pConfigMemory[offset % m_device.GetConfSize()];
	}
	offset = addr;
	return m_pCodeMemory[offset % m_device.GetProgSize()];
}

bool PSoC3Memory::readFromFile(FILE *fp)
{
	hexline hex(fp);
	unsigned long addr;
	unsigned char count,data[255];
	while (hex.readData(addr,count,data)){
		for (int i=0;i<count;i++){
			store(addr++,data[i]);
		}
	}
	return !hex.error();
}

void PSoC3Memory::writedata(hexline &hex,unsigned long addr,unsigned long count,unsigned char *data)
{
	unsigned char buf[16];
	bool ignore;
	int i,j,k;
	for (i=0;i<count;){
		k=i;
		ignore=true;
		for (j=0;(j<16) && (i<count);i++,j++){
			if (*data==0xff)
				;
			else
				ignore=false;
			buf[j]=*data++;
		}
//		if (!ignore){
//			for (j=16;j>0;j--)
//				if (buf[j-1]==0xff);
//				else break;
			hex.writeData(addr+k,j,buf);
//		}
	}
}

bool PSoC3Memory::saveToFile(FILE *fp)
{
	hexline hex(fp);
/*	
	fprintf(fp, ":020000040000FA\n");
	
	writedata(hex, 0x000000, m_device.GetProgSize(), m_pCodeMemory);
	writedata(hex, 0x200000, sizeof(m_UserID), m_UserID);
	writedata(hex, 0x300000, sizeof(m_Configuration), m_Configuration);
	writedata(hex, 0x3ffffe, sizeof(m_DeviceID), m_DeviceID);
	if (m_device.GetConfSize()){
		writedata(hex, 0xf00000, m_device.GetConfSize(),m_pDataMemory);
	}
	
	fprintf(fp, ":00000001FF\n");
	*/
	return true;
}

void PSoC3Memory::setTestData(void)
{
	for(int i=0;i<m_device.GetProgSize();i++){
		store(0x000000 + i,~i);
	}
	for(int i=0;i<m_device.GetConfSize();i++){
		store(0xf00000 + i,~i);
	}
	//	for(int i=0;i<m_device.GetProgSize();i++){
//		m_pCodeMemory[i] = i;
//	}
//	for(int i=0;i<m_device.GetConfSize();i++){
//		m_pDataMemory[i] = i;
//	}
}

