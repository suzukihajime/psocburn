#ifndef PICMEMORY_H
#define PICMEMORY_H

#include <stdio.h>

class PICDevice;
class PICMemory
{
protected:
	PICDevice &m_device;
	
public:
	PICMemory(PICDevice &device) : m_device(device) {}
	virtual ~PICMemory() {}
	
	virtual bool readFromFile(FILE *fp) = 0;
	virtual bool saveToFile(FILE *fp) = 0;
	virtual void setTestData(void){}
};

#endif
