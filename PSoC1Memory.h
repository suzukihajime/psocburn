// PSoC1Memory.h
//    2012.2 aasoukai128

#ifndef PSoC1MEMORY_H
#define PSoC1MEMORY_H

#include "PICMemory.h"

#define MAX_PROGRAM_SIZE 0x4000

class hexline;

class PSoC1Memory : public PICMemory
{
	unsigned short *m_pMemoryBuffer;
	
	void writeWords(hexline &hex,int begin,int end,unsigned short msk);
	
public:
	
	PSoC1Memory(PICDevice &device)
		: PICMemory(device) {
		m_pMemoryBuffer = new unsigned short[MAX_PROGRAM_SIZE];
		for (int i=0;i<MAX_PROGRAM_SIZE;i++)
			m_pMemoryBuffer[i]=0xffff;
	}
	
	~PSoC1Memory(){
		if (m_pMemoryBuffer)
			delete [] m_pMemoryBuffer;
	}
	
	bool readFromFile(FILE *fp);
	bool saveToFile(FILE *fp);
	void setTestData(void);
	
	unsigned short& operator[](unsigned long address){
		return m_pMemoryBuffer[address];
	}
};

#endif
