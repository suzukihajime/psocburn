// PSoC5Strategy.cc
//    2012.8 aasoukai128

#include "PICDevice.h"
#include "PSoC5Strategy.h"
#include "progress.h"

PSoC5Strategy::PSoC5Strategy(PICDevice &dev,PICProgrammer &prog)
	: PICStrategy(dev)
	, m_memory(dev)
	, m_piccmd(prog)
{
}

PSoC5Strategy::~PSoC5Strategy()
{
}

bool PSoC5Strategy::reset(void)
{
	m_piccmd.exitDebugMode();
	return(m_piccmd.enterDebugMode());
}

bool PSoC5Strategy::final()
{
	m_piccmd.exitDebugMode();
	return true;
}

bool PSoC5Strategy::deviceCheck()
{
	if(!reset())
	{
		fprintf(stderr, "[ERROR]: Device cannot be acquired\n");
		return false;
	}
	if (m_device.GetDeviceID()){
//	if (m_memory.getJtagID()){
		unsigned int devid;
		int ret = 0;
		ret |= !m_piccmd.loadDataForApacc(0x400046e8, 0x00000002);
		m_piccmd.readDataFromDpacc(&devid);
		ret |= !m_piccmd.loadDataForApacc(0x00050203, 0x00000020);
		m_piccmd.switchToSWD();
		if(m_device.GetDeviceID() == devid && ret == 0) {
			return true;
		} else {
//		if(m_memory.getJtagID() != devid){
			fprintf(stderr, "[ERROR]: Device not match\n");
			return false;
		}
	}
	return false;
}


bool PSoC5Strategy::chipErase()
{
	progress progress1;
	bool ret;
	
	progress1.begin("Chip Erase...");
	/* erase start */
	ret = !m_piccmd.loadDataForApacc(0x40004720, 0x000000b6);	// First initiation key
	ret |= !m_piccmd.loadDataForApacc(0x40004720, 0x000000dc);	// Second key:00DC(0xD3 + 0x09); 0x09 is Erase All opcode
	ret |= !m_piccmd.loadDataForApacc(0x40004720, 0x00000009);	// erase all opcode
	ret |= !m_piccmd.pollDataInApacc(0x40004722, 0x00000002);
	progress1.end("Done");
	if(!ret) {
		return true;
	} else {
		fprintf(stderr, "[ERROR]: erase failed\n");
		return false;
	}
}


bool PSoC5Strategy::writeMemory(unsigned char *buf, unsigned char *conf, int rowCount, int arrayCount)
{
	int count, data, blockSize, rowLength, ret = 0;
	
	blockSize = m_device.GetBlockSize();
	rowLength = m_device.GetBlockSize() + m_device.GetEccBlockSize();
	// write even
	m_piccmd.beginCommand();
	ret = !m_piccmd.loadDataForApacc(0x20000000, 0x0002d5b6 | (arrayCount<<24));
	for(count = 0; count < rowLength; count += 4)
	{
		if(count < blockSize) {
			data =    (buf[count + 0])
					| (buf[count + 1]<<8)
					| (buf[count + 2]<<16)
					| (buf[count + 3]<<24);
		} else {
			data =    (conf[count - blockSize + 0])
					| (conf[count - blockSize + 1]<<8)
					| (conf[count - blockSize + 2]<<16)
					| (conf[count - blockSize + 3]<<24);
		}
		m_piccmd.loadDataForApacc(0x20000004 + count, data);
	}
	m_piccmd.loadDataForApacc(rowLength + 0x20000004, 0xb6000000);
	m_piccmd.loadDataForApacc(rowLength + 0x20000008, (arrayCount<<16) + 0x000005d8);
	m_piccmd.loadDataForApacc(rowLength + 0x2000000c, 0x00190100 | (rowCount & 0x000000ff));
	m_piccmd.loadDataForApacc(0x40007018, 0x00000000);
	m_piccmd.loadDataForApacc(0x40007010, 0x00000021);
	m_piccmd.loadDataForApacc(0x40007600, 0x00000080);
	m_piccmd.loadDataForApacc(0x40007604, 0x40002000);
	m_piccmd.loadDataForApacc(0x40007800, 0x01ff0000 + rowLength + 15);
	m_piccmd.loadDataForApacc(0x40007804, 0x47200000);
	m_piccmd.endCommand();
	// wait until SPC has done previous request
	ret = !m_piccmd.pollDataInApacc(0x40004722, 0x00000002);
	
	m_piccmd.loadDataForApacc(0x40007014, 0x00000001);
	
	// write odd
	rowCount++;
	buf += m_device.GetBlockSize();
	conf += m_device.GetEccBlockSize();
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x20000200, 0x0002d5b6 | (arrayCount<<24));
	for(count = 0; count < rowLength; count += 4)
	{
		if(count < blockSize) {
			data =    (buf[count + 0])
					| (buf[count + 1]<<8)
					| (buf[count + 2]<<16)
					| (buf[count + 3]<<24);
		} else {
			data =    (conf[count - blockSize + 0])
					| (conf[count - blockSize + 1]<<8)
					| (conf[count - blockSize + 2]<<16)
					| (conf[count - blockSize + 3]<<24);
		}
		m_piccmd.loadDataForApacc(0x20000204 + count, data);
	}
	m_piccmd.loadDataForApacc(rowLength + 0x20000204, 0xb6000000);
	m_piccmd.loadDataForApacc(rowLength + 0x20000208, (arrayCount<<16) + 0x000005d8);
	m_piccmd.loadDataForApacc(rowLength + 0x2000020c, 0x00190100 | (rowCount & 0x000000ff));
	m_piccmd.loadDataForApacc(0x40007028, 0x00000100);
	m_piccmd.loadDataForApacc(0x40007020, 0x00000021);
	m_piccmd.loadDataForApacc(0x40007608, 0x00000080);
	m_piccmd.loadDataForApacc(0x4000760c, 0x40002000);
	m_piccmd.loadDataForApacc(0x40007808, 0x01ff0000 + rowLength + 15);
	m_piccmd.loadDataForApacc(0x4000780c, 0x47200200);
	m_piccmd.endCommand();
	// wait until SPC has done previous request
	ret |= !m_piccmd.pollDataInApacc(0x40004722, 0x00000002);
	
	m_piccmd.loadDataForApacc(0x40007024, 0x00000001);
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

bool PSoC5Strategy::verifyMemory(unsigned char *buf, unsigned char *conf, int rowCount, int arrayCount)
{
	int address, ret = 0;
	unsigned char *rbuf, *rconf;
	rbuf = new unsigned char[m_device.GetBlockSize()];
	rconf = new unsigned char[m_device.GetEccBlockSize()];
	
	address = rowCount * m_device.GetBlockSize();
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x40004720, 0x000000b6);
	m_piccmd.loadDataForApacc(0x40004720, 0x000000d7);
	m_piccmd.loadDataForApacc(0x40004720, 0x00000004);
	m_piccmd.loadDataForApacc(0x40004720, arrayCount);
	m_piccmd.loadDataForApacc(0x40004720, (address>>16) & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, (address>>8)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, (address>>0)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, 0x000000ff);
	m_piccmd.endCommand();
	ret |= !m_piccmd.pollDataInApacc(0x40004722, 0x00000001);
	/* read multiple bytes */
	ret |= !m_piccmd.readMultipleBytesFromApacc(0x40004720, rbuf, m_device.GetBlockSize());	
	for(int i = 0; i < m_device.GetBlockSize(); i++) {
		ret |= (rbuf[i] != buf[i]);
	}
	
	address = rowCount * m_device.GetEccBlockSize() | 0x00800000;
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x40004720, 0x000000b6);		// first initiation key
	m_piccmd.loadDataForApacc(0x40004720, 0x000000d7);		// 0xd2 + READ_MULTIPLE_BYTE opcode
	m_piccmd.loadDataForApacc(0x40004720, 0x00000004);		// READ_MULTIPLE_BYTE opcode
	m_piccmd.loadDataForApacc(0x40004720, arrayCount);		// Arrar ID
	m_piccmd.loadDataForApacc(0x40004720, (address>>16) & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, (address>>8)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, (address>>0)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, 0x0000001f);
	m_piccmd.endCommand();
	ret |= !m_piccmd.pollDataInApacc(0x40004722, 0x00000001);
	ret |= !m_piccmd.readMultipleBytesFromApacc(0x40004720, rconf, m_device.GetEccBlockSize());
	for(int i = 0; i < m_device.GetEccBlockSize(); i++) {
//		ret |= (rconf[i] != conf[i]);
	}
	delete [] rbuf;
	delete [] rconf;
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

bool PSoC5Strategy::readMemory(unsigned char *buf, unsigned char *conf, int rowCount, int arrayCount)
{
	int rowLength, address, ret = 0;	
	rowLength = m_device.GetBlockSize() + m_device.GetEccBlockSize();
	address = rowCount * m_device.GetBlockSize();
	
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x40004720, 0x000000b6);
	m_piccmd.loadDataForApacc(0x40004720, 0x000000d7);
	m_piccmd.loadDataForApacc(0x40004720, 0x00000004);
	m_piccmd.loadDataForApacc(0x40004720, arrayCount);
	m_piccmd.loadDataForApacc(0x40004720, (address>>16) & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, (address>>8)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, (address>>0)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, 0x000000ff);
	m_piccmd.endCommand();
	ret |= !m_piccmd.pollDataInApacc(0x40004722, 0x00000001);
	/* read multiple bytes */
	ret |= !m_piccmd.readMultipleBytesFromApacc(0x40004720, buf, m_device.GetBlockSize());
	
	address = rowCount * m_device.GetEccBlockSize() | 0x00800000;
	m_piccmd.beginCommand();
	m_piccmd.loadDataForApacc(0x40004720, 0x000000b6);		// first initiation key
	m_piccmd.loadDataForApacc(0x40004720, 0x000000d7);		// 0xd2 + READ_MULTIPLE_BYTE opcode
	m_piccmd.loadDataForApacc(0x40004720, 0x00000004);		// READ_MULTIPLE_BYTE opcode
	m_piccmd.loadDataForApacc(0x40004720, arrayCount);		// Arrar ID
	m_piccmd.loadDataForApacc(0x40004720, (address>>16) & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, (address>>8)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, (address>>0)  & 0x000000ff);
	m_piccmd.loadDataForApacc(0x40004720, 0x0000001f);
	ret |= !m_piccmd.pollDataInApacc(0x40004722, 0x00000001);
	ret |= !m_piccmd.readMultipleBytesFromApacc(0x40004720, conf, m_device.GetEccBlockSize());
	if(!ret) {
		return true;
	} else {
		return false;
	}
}

unsigned int PSoC5Strategy::readChecksum(int arrayCount)
{
	unsigned int checksum;
	int block, array, prog, rowsPerArray, ret = 0;
	unsigned char rbuf[4];
	
	block = m_device.GetBlockSize();
	array = m_device.GetArraySize();
	prog = m_device.GetProgSize();
	if(arrayCount == (prog/(array*block))) {
		rowsPerArray = m_device.GetProgSize() - arrayCount*array*block;
	} else {
		rowsPerArray = 256;
	}
	m_piccmd.loadDataForApacc(0x40004720, 0x000000b6);
	m_piccmd.loadDataForApacc(0x40004720, 0x000000df);
	m_piccmd.loadDataForApacc(0x40004720, 0x0000000c);
	m_piccmd.loadDataForApacc(0x40004720, arrayCount);
	m_piccmd.loadDataForApacc(0x40004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x40004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x40004720, 0x00000000);
	m_piccmd.loadDataForApacc(0x40004720, (rowsPerArray-1) & 0xff);
	ret |= !m_piccmd.pollDataInApacc(0x40004722, 0x00000001);
	m_piccmd.readMultipleBytesFromApacc(0x40004720, rbuf, 4);
	ret |= !m_piccmd.pollDataInApacc(0x40004722, 0x00000002);
	checksum =    (unsigned int)rbuf[0]<<24
				| (unsigned int)rbuf[1]<<16
				| (unsigned int)rbuf[2]<<8
				| (unsigned int)rbuf[3];
	return checksum;
}

/*
unsigned int PSoC5Strategy::checkTemperature()
{
	unsigned int temp;
	unsigned char rbuf[2];
	m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
	m_piccmd.loadDataForApacc(0x00004720, 0x000000e1);
	m_piccmd.loadDataForApacc(0x00004720, 0x0000000e);
	m_piccmd.loadDataForApacc(0x00004720, 0x00000003);
	m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
	m_piccmd.readMultipleBytesFromApacc(0x00004720, rbuf, 2);
	m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	temp = (((int)rbuf[0]<<8) & 0x0000ff00) | (((int)rbuf[1]<<16) & 0x00ff0000);
	return(temp);
}*/
/*
bool PSoC5Strategy::writeNVLatch(unsigned char *buf, int size)
{
	unsigned int *rbuf = new unsigned int[m_device.GetNvlSize()];
	int rewrite = 0;
	bool ret;
	
//	fprintf(stderr, "Checking NVL...\n");
	// read nvl from device
	ret = !m_piccmd.loadDataForApacc(0x00005112, 0x00000000);
	m_piccmd.loadDataForApacc(0x00005113, 0x00000004);
	m_piccmd.loadDataForApacc(0x00005114, 0x00000000);
	m_piccmd.loadDataForApacc(0x00005110, 0x00000004);
	for(int i = 0; i < m_device.GetNvlSize(); i++)
	{
		m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
		m_piccmd.loadDataForApacc(0x00004720, 0x000000d6);
		m_piccmd.loadDataForApacc(0x00004720, 0x00000003);
		m_piccmd.loadDataForApacc(0x00004720, 0x00000080);
		m_piccmd.loadDataForApacc(0x00004720, i);
		ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000001);
		m_piccmd.readDataFromApacc(0x00004720, &rbuf[i]);
		ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
	}
	
	// check if rewrite is needed
	for(int i = 0; i < m_device.GetNvlSize(); i++) {
		rewrite |= (buf[i] == (unsigned char)rbuf[i]) ? 0 : 1;
	}
	
	// rewrite nvl memory
	if(rewrite)
	{
		progress progress1;
		progress1.begin("Rewrite NVL");
		progress1.setmax(m_device.GetNvlSize());
		for(int i = 0; i < m_device.GetNvlSize(); i++)
		{
			m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
			m_piccmd.loadDataForApacc(0x00004720, 0x000000d3);
			m_piccmd.loadDataForApacc(0x00004720, 0x00000000);
			m_piccmd.loadDataForApacc(0x00004720, 0x00000080);
			m_piccmd.loadDataForApacc(0x00004720, i);
			m_piccmd.loadDataForApacc(0x00004720, buf[i]);
			ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
			progress1.setval(i);
		}
		
		m_piccmd.loadDataForApacc(0x00004720, 0x000000b6);
		m_piccmd.loadDataForApacc(0x00004720, 0x000000d9);
		m_piccmd.loadDataForApacc(0x00004720, 0x00000006);
		m_piccmd.loadDataForApacc(0x00004720, 0x00000080);
		ret |= !m_piccmd.pollDataInApacc(0x00004722, 0x00000002);
		progress1.end("Done");
		// check if ecc Enable is changed
	}
	else
	{
		fprintf(stderr, "No need to rewrite NVL\n");
	}
	delete [] rbuf;
	return(ret ? false : true);
}
*/
bool PSoC5Strategy::readBuffer()
{
	progress progress1;
	int size, block, array;
	
	size = m_device.GetProgSize();
	block = m_device.GetBlockSize();
	array = m_device.GetArraySize();
	progress1.begin("Program Area Reading");
	progress1.setmax(size);
	//progress1.end("Done");
	for(int addr = 0; addr < size; addr += block) {
		if(!readMemory(&m_memory[addr], &m_memory[0x80000000 + addr/8], (addr/block) & 0xff, addr/(array*block))) { 		// magic number '8' fixme
			fprintf(stderr, "[ERROR] Reading memory failed - %d\n", addr/block);
			return false;
		}
		progress1.setval(addr);
	}
	progress1.end("Done");
	
	return true;
}

bool PSoC5Strategy::writeBuffer()
{
	progress progress1;
	int size, block, array, temp;
	
	// first write none volatile latch
	//size = m_device.GetNvlSize();
	//writeNVLatch(&m_memory[0x90000000], size);
	
	// then write program flash memory
	// first Get temperature twice
/*	if(m_device.GetSiliconRev() >= 3) {		// Silicon ES3 or greater only
		checkTemperature();
		temp = checkTemperature();
	}*/
	// check program memory size and program block size
	size = m_device.GetProgSize();
	block = m_device.GetBlockSize();
	array = m_device.GetArraySize();
	progress1.begin("Program Area Writing");
	progress1.setmax(size);
	m_piccmd.setDMAAccessWidth(4);
	for(int addr = 0; addr < size; addr += block*2){
		if(!writeMemory(&m_memory[addr], &m_memory[0x80000000 + addr/8], (addr/block) & 0xff, addr/(block*array))) {		// magic number '8' fix me
			fprintf(stderr, "[ERROR] Program memory failed\n");
			return false;
		}
		progress1.setval(addr);
	}
	m_piccmd.pollDataInApacc(0x40004722, 0x00000002);
	if(!verifyChecksum()) {
		progress1.end("Failed (Checksum not match)");
	} else {
		progress1.end("Done");
	}
	return true;
}

bool PSoC5Strategy::verifyBuffer()
{
	progress progress1;
	int size, block, array;
	
	size = m_device.GetProgSize();
	block = m_device.GetBlockSize();
	array = m_device.GetArraySize();
	progress1.begin("Program Area Verifing");
	progress1.setmax(size);
	
	for(int addr = 0; addr < size; addr += block) {
		if(!verifyMemory(&m_memory[addr], &m_memory[0x80000000 + addr/8], (addr/block) & 0xff, addr/(array*block))) {		// magic number '8' fixme
			fprintf(stderr, "[ERROR] Verify failed\n");
			return false;
		}
		progress1.setval(addr);
	}
	progress1.end("Done");
	
	return true;
}

bool PSoC5Strategy::verifyChecksum()
{
	int size, block, array, checksum = 0;;
	
	size = m_device.GetProgSize();
	block = m_device.GetBlockSize();
	array = m_device.GetArraySize();
	for(int addr = 0; addr < size; addr += array*block) {
		checksum += readChecksum(addr/(array*block));
	}
	checksum &= 0xffff;
	if(checksum != ((int)(m_memory[0x90300000]<<8 | m_memory[0x90300001]) & 0xffff)) {
		return false;
	} else {
		return true;
	}
}
