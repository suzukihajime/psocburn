// PSoC1Memory.cc
//    2012.2 aasoukai128

#include "PSoC1Memory.h"
#include "PICDevice.h"
#include "hexline.h"

bool PSoC1Memory::readFromFile(FILE *fp)
{
	hexline hex(fp);
	unsigned long addr;
	unsigned char count,data[256];
/*
	while (hex.readData(addr,count,data)){
		addr /= 2;
		for (int i=0;i<count;i+=2){
			m_pMemoryBuffer[addr++] = (data[i+0]) | (data[i+1]<<8);
		}
	}
	
	if (hex.error()){
		;
	}else{
		unsigned short *p,size,mask;
		
		p = &m_pMemoryBuffer[0];
		size = m_device.GetProgSize();
		mask = m_device.GetPMMask();
		for (int i=0;i<size;i++){
			*p++&=mask;
		}
		
		p = &m_pMemoryBuffer[m_device.GetIDAddr()];
		size = m_device.GetConfSize();
		mask = m_device.GetPMMask();
		for (int i=0;i<size;i++){
			*p++&=mask;
		}
		
		if (m_device.GetConfAddr1()<0x2000){
			m_pMemoryBuffer[0xfff] &= mask;
			m_pMemoryBuffer[m_device.GetConfAddr1()] = m_pMemoryBuffer[0xfff];
		}
		
		p = &m_pMemoryBuffer[0x2100];
		size = m_device.GetDataSize();
		mask = 0xff;
		for (int i=0;i<size;i++){
			*p++&=mask;
		}
	}
*/	
	return !hex.error();
}

void PSoC1Memory::writeWords(hexline &hex,int begin,int end,unsigned short msk)
{
	unsigned char buf[16];
	bool ignore;
	int i,j,k;
	for (i=begin;i<end;){
		k=i;
		ignore=true;
		for (j=0;j<16;j+=2){
			buf[j+0]=(msk&0xff);
			buf[j+1]=(msk>>8);
		}
		for (j=0;(j<16) && (i<end);i++,j+=2){
			unsigned short word=m_pMemoryBuffer[i]&msk;
			if (word==msk){
				;
			}else{
				ignore=false;
			}
			buf[j+0]=(word&0xff);
			buf[j+1]=(word>>8);
		}
// 		if (!ignore){
// 			for (j=16;j>0;j-=2)
// 				if ((buf[j-2]==(msk&0xff)) && (buf[j-1]==(msk>>8)) );
// 				else break;
			hex.writeData(k*2,j,buf);
// 		}
	}
}

bool PSoC1Memory::saveToFile(FILE *fp)
{
	hexline hex(fp);
/*	
	//
	fprintf(fp, ":020000040000FA\n");
	
	// Program Memory
	writeWords(hex,
			   0,
			   m_device.GetProgSize(),
			   m_device.GetPMMask());
	
	// Configuration Word & UserID
	writeWords(hex,
			   m_device.GetIDAddr(),
			   m_device.GetIDAddr()+m_device.GetConfSize(),
			   m_device.GetPMMask());
	
	// Configuration Word(0x3ff,0x1ff...)
	if (m_device.GetConfAddr1() >= 0x2000){
		;
	}else{
		m_pMemoryBuffer[0xfff] = m_pMemoryBuffer[m_device.GetConfAddr1()];
		writeWords(hex,
				   0xfff,
				   0xfff+1,
				   m_device.GetPMMask());
	}
	
	// DATA Memory
	writeWords(hex,
			   0x2100,
			   0x2100+m_device.GetDataSize(),
			   0xff);
	
	// end record
	fprintf(fp, ":00000001FF\n");
*/	
	return true;
}

void PSoC1Memory::setTestData(void)
{
	for (int i=0;i<m_device.GetProgSize();i++)
	   	m_pMemoryBuffer[i]=~i;
	for (int i=0;i<m_device.GetConfSize();i++)
	   	m_pMemoryBuffer[0x2100+i]=~i;
}
