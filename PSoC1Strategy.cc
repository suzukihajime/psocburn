// PSoC1Strategy.cc
//    2012.2 aasoukai128

#include "PICDevice.h"
#include "PSoC1Strategy.h"
#include "progress.h"


PSoC1Strategy::PSoC1Strategy(PICDevice &dev,PICProgrammer &prog)
	: PICStrategy(dev)
	, m_memory(dev)
	, m_piccmd(prog)
{
}

PSoC1Strategy::~PSoC1Strategy()
{
}

bool PSoC1Strategy::reset(void)
{
	m_piccmd.exitDebugMode();
	m_piccmd.enterDebugMode();
	return true;
}

bool PSoC1Strategy::final()
{
	m_piccmd.exitDebugMode();
	return true;
}

bool PSoC1Strategy::deviceCheck()
{
	reset();
	/*
	* Device ID check
	*/
	if (m_device.GetDeviceID()){
		unsigned short devid = 0;
		unsigned char rbuf;
		
		m_piccmd.writeCommand(0xf7, 0x10);
		m_piccmd.writeCommand(0xe0, 0x02);
		m_piccmd.writeCommand(0xf7, 0x00);
		m_piccmd.writeCommand(0xf6, 0x00);
		m_piccmd.writeData(0xf8, 0x3a);
		m_piccmd.writeData(0xf9, 0x03);
		m_piccmd.writeCommand(0xf5, 0x00);
		m_piccmd.writeCommand(0xf4, 0x03);
		m_piccmd.writeData(0xfb, 0x80);
		m_piccmd.writeCommand(0xf9, 0x30);
		m_piccmd.writeCommand(0xfa, 0x40);
		m_piccmd.writeData(0xfa, 0x00);
		m_piccmd.writeCommand(0xf0, 0x06);
		m_piccmd.writeCommand(0xf8, 0x00);
		m_piccmd.writeCommand(0xff, 0x12);
		
		m_piccmd.readData(0xf8, &rbuf);
		devid = (unsigned short)rbuf<<8;
		m_piccmd.readData(0xf9, &rbuf);
		devid |= (unsigned short)rbuf & 0xff;
		
		if(m_device.GetDeviceID() != devid){
			fprintf(stderr, "[ERROR]: device not match\n");
			return false;
		}
	}
	
	return true;
}


bool PSoC1Strategy::chipErase()
{
	progress progress1;
	
//	reset();
	progress1.begin("Chip Erase...");
	/* erase */
	m_piccmd.writeData(0xfc, 0x15);
	m_piccmd.writeData(0xfe, 0x56);
	m_piccmd.writeCommand(0xf7, 0x00);
	m_piccmd.writeCommand(0xf6, 0x00);
	m_piccmd.writeData(0xf8, 0x3a);
	m_piccmd.writeData(0xf9, 0x03);
	m_piccmd.writeCommand(0xf5, 0x00);
	m_piccmd.writeCommand(0xf4, 0x03);
	m_piccmd.writeData(0xfb, 0x80);
	m_piccmd.writeCommand(0xf9, 0x30);
	m_piccmd.writeCommand(0xfa, 0x40);
	m_piccmd.writeCommand(0xf0, 0x05);
	m_piccmd.writeCommand(0xf8, 0x00);
	m_piccmd.writeCommand(0xff, 0x12);
	
	progress1.end("Done");
	
	return true;
}


bool PSoC1Strategy::writeMemory(unsigned short *buf,int size)
{
	int c,b,e,w;
	unsigned short m;
/*
	if (m_address < 0x2100){	// program/config memory
		c = PICProgrammer::load_prog;		// load cmd
		m = m_device.GetPMMask();			   // memory mask
		if (m_address < m_device.GetIDAddr()){ // program memory
			w = m_device.GetProgWait();		   // program wait
//			write_param(m_device.GetProgType(),b,e); // begin/end programing cmd
		}else{					// config memory
			w = m_device.GetProgWaitC();
//			write_param(m_device.GetProgTypeC(),b,e);
		}
	}else{						// data memory
		c = PICProgrammer::load_data;
		m = 0x00ff;
		w = m_device.GetProgWaitD();
//		write_param(m_device.GetProgType(),b,e);
	}
	int i,k;
	for (i=k=0;i<size;i++){
		if ((buf[i] & m) == m) k++;
	}
	if (k == size){
//		m_piccmd.incrementAddress(size); // skip write
	}else{
		m_piccmd.wordWriteCycle(c,b,e,w,buf,size);
	}
	m_address += size;	*/
	return true;
}

bool PSoC1Strategy::readMemory(unsigned short *buf,int size)
{
	bool ret;
	if (m_address < 0x2100){
		ret = m_piccmd.wordReadCycle(PICProgrammer::read_prog, buf, size);
	}else{
		ret = m_piccmd.wordReadCycle(PICProgrammer::read_data, buf, size);
	}
	if (ret)
		m_address += size;
	return ret;
}

bool PSoC1Strategy::verifyMemory(unsigned short *buf,int size,unsigned short mask)
{
	char *format;
	unsigned short vbuf[size];
	bool ret;
	/*
	if (m_address < 0x2100){
		ret = m_piccmd.wordReadCycle(PICProgrammer::read_prog, vbuf, size);
		if (m_address < m_device.GetIDAddr())
			format = "\nVerify Error on Program Area of %04x: %04x - %04x\n";
		else
			format = "\nVerify Error on Config Area of %04x: %04x - %04x\n";
	}else{
		ret = m_piccmd.wordReadCycle(PICProgrammer::read_data, vbuf, size);
		format = "\nVerify Error on Data Area of %04x: %04x - %04x\n";
	}
	for (int i=0;i<size;i++){
		if ((vbuf[i] & mask) != (buf[i] & mask)){
			fprintf(stderr, format, m_address+i, (vbuf[i]&mask), (buf[i]&mask));
		}
	}
	if (ret)
		m_address += size;*/
	return ret;
}

bool PSoC1Strategy::readBuffer()
{
	progress progress1;
	int size,rsize;
	
	progress1.begin("Program Area Reading");
	/*
//	setaddress(0);
	
	size = m_device.GetProgSize();
	rsize = 64;
	
	progress1.setmax(size);
	for(int i=0;i<size;i+=rsize){
		if((i+rsize) > size) rsize = size - i;
		readMemory(&m_memory[i], rsize);
		progress1.setval(i);
	}
	
	progress1.end("Done");
	
	
	size = m_device.GetDataSize();
	if (size){
		progress1.begin("Data Area Reading");
		progress1.setmax(size);
		
//		setaddress(0x2100);
		rsize = 64;
		for(int i=0;i<size;i+=rsize){
			if((i+rsize) > size) rsize = size - i;
			readMemory(&m_memory[0x2100+i], rsize);
			progress1.setval(i);
		}

		progress1.end("Done");
	}
	
	progress1.begin("Config Area Reading");
	fprintf(stderr,".");
	
//	setaddress(m_device.GetIDAddr());
	readMemory(&m_memory[m_device.GetIDAddr()], m_device.GetConfSize());
	fprintf(stderr,".");
	
	if (m_device.GetConfAddr1() < 0x2000){ // for baseline
//		setaddress(m_device.GetConfAddr1());
		readMemory(&m_memory[m_device.GetConfAddr1()], 1);
		fprintf(stderr,".");
	}
	*/
	progress1.end("Done");
	
	return true;
}

bool PSoC1Strategy::writeBuffer()
{
	progress progress1;
	int size,latch;
	unsigned long addr,calibaddr;
	/*
	size = m_device.GetProgSize();
	latch = m_device.GetProgLatch();
	
	calibaddr = m_device.GetCalibAddr(0);
	if (calibaddr && (calibaddr<size)) {
		unsigned short calib;
//		setaddress(calibaddr);
		readMemory(&calib,1);
		m_memory[calibaddr] = calib;
	}
	
	progress1.begin("Program Area Writing");
	progress1.setmax(size);
	
//	setaddress(0);
	for (addr=0;size>=latch;addr+=latch,size-=latch){
		writeMemory(&m_memory[addr], latch);
		progress1.setval(addr);
	}
	for (int i=0;i<size;i++,addr++){
		writeMemory(&m_memory[addr], 1);
		progress1.setval(addr);
	}
	
	progress1.end("Done");
	
	
	size = m_device.GetDataSize();
	if (size){
		progress1.begin("Data Area Writing");
		progress1.setmax(size);
		
//		setaddress(0x2100);
		for (int i=0;i<size;i++){
			writeMemory(&m_memory[0x2100+i], 1);
			progress1.setval(i);
		}
		
		progress1.end("Done");
	}
	
	
	progress1.begin("Config Area Writing");
	
//	setaddress(m_device.GetIDAddr());
	for (int i=0;i<4;i++){
		writeMemory(&m_memory[m_device.GetIDAddr()+i], 1);
		fprintf(stderr,".");
	}
	
	unsigned short conf;
//	setaddress(m_device.GetConfAddr1());
//	m_piccmd.readDataFromProgramMemory(&conf);
	conf &= ~m_device.GetConfMask1(); // keep band gap bits
	conf |= (m_memory[m_device.GetConfAddr1()] & m_device.GetConfMask1());
	writeMemory(&conf, 1);
	fprintf(stderr,".");
	
	if (m_device.GetConfAddr2()){
//		setaddress(m_device.GetConfAddr2());
		writeMemory(&m_memory[m_device.GetConfAddr2()], 1);
		fprintf(stderr,".");
	}
	
	progress1.end("Done");
	*/
	return true;
}

bool PSoC1Strategy::verifyBuffer()
{
	progress progress1;
	int size,rsize;
	/*
	size = m_device.GetProgSize();
	rsize = 64;
	
	if (m_device.GetCalibAddr(0) &&
		m_device.GetCalibAddr(0) < size) // do not verify OSCCAL word
		size--;
	
	progress1.begin("Program Area Verifing");
	progress1.setmax(size);
	
//	setaddress(0);
	for(int i=0;i<size;i+=rsize){
		if((i+rsize) > size) rsize = size - i;
		verifyMemory(&m_memory[i], rsize, m_device.GetPMMask());
		progress1.setval(i);
	}
	
	progress1.end("Done");
	
	
	size = m_device.GetDataSize();
	if (size){
		progress1.begin("Data Area Verifing");
		progress1.setmax(size);
		
//		setaddress(0x2100);
		rsize = 64;
		for(int i=0;i<size;i+=rsize){
			if((i+rsize) > size) rsize = size - i;
			verifyMemory(&m_memory[0x2100+i], rsize, 0xff);
			progress1.setval(i);
		}
		
		progress1.end("Done");
	}
	
	
	progress1.begin("Config Area Verifing");
	
//	setaddress(m_device.GetIDAddr());
	verifyMemory(&m_memory[m_device.GetIDAddr()], 4, 0xf);
	fprintf(stderr,".");
	
//	setaddress(m_device.GetConfAddr1());
	verifyMemory(&m_memory[m_device.GetConfAddr1()], 1, m_device.GetConfMask1());
	fprintf(stderr,".");
	
	if (m_device.GetConfAddr2()){
//		setaddress(m_device.GetConfAddr2());
		verifyMemory(&m_memory[m_device.GetConfAddr2()], 1, m_device.GetConfMask2());
		fprintf(stderr,".");
	}
	
	progress1.end("Done");
	*/
	return true;
}
