#ifndef HEXLINE_H
#define HEXLINE_H

#include <stdio.h>

class hexline
{
	FILE*	_fileHandle;
	int		_highAddress;
	int		_lineCount;
	bool 	_error;
	
public:
	hexline(FILE *f)
		:_fileHandle(f)
		,_highAddress(0)
		,_lineCount(0)
		,_error(0)
	{}
	
	virtual ~hexline(){}
	
	bool readData(unsigned long &addr,unsigned char &length,unsigned char *data);
	bool writeData(unsigned long addr,unsigned char count,const unsigned char *data);
	bool getline(int &count,int &offset,int &type,unsigned char *data);
	bool putline(int count,int offset,int type,const unsigned char *data);
	bool error(void){
		return _error;
	}
};

#endif
