// PSoC5Strategy.h
//    2012.8 aasoukai128

#ifndef PSoC5STRATEGY_H
#define PSoC5STRATEGY_H

#include "PICStrategy.h"
#include "PSoC5Memory.h"
#include "PSoC5Command.h"

class PSoC5Strategy : public PICStrategy
{
	PSoC5Memory m_memory;
	PSoC5Command m_piccmd;
	PICProgrammer::PowerSequence m_enterseq;
	PICProgrammer::PowerSequence m_exitseq;
	unsigned long  m_address;
	unsigned short m_osccal[4];
	
public:
	PSoC5Strategy(PICDevice &dev,PICProgrammer &prog);
	~PSoC5Strategy();
	
	bool final();
	bool deviceCheck();
	bool chipErase();
	bool readBuffer();
	bool writeBuffer();
	bool verifyBuffer();
	
	PSoC5Memory* memory() { return &m_memory; }
	
private:
	bool reset(void);
	bool writeMemory(unsigned char *buf, unsigned char *conf, int rowCount, int arrayCount);
	bool readMemory(unsigned char *buf, unsigned char *conf, int rowCount, int arrayCount);
	bool verifyMemory(unsigned char *buf, unsigned char *conf, int rowCount, int arrayCount);
	unsigned int readChecksum(int arrayCount);
	bool verifyChecksum(void);
	unsigned int checkTemperature(void);
	bool writeNVLatch(unsigned char *buf, int size);
};

#endif
