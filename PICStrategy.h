#ifndef PICSTRATEGY_H
#define PICSTRATEGY_H

class PICDevice;
class PICMemory;
class PICProgrammer;

class PICStrategy
{
protected:
	PICDevice &m_device;
	
public:
	PICStrategy(PICDevice &dev) : m_device(dev) {}
	virtual ~PICStrategy() {}
	
	virtual bool final() = 0;
	
	virtual bool deviceCheck() = 0;
	virtual bool chipErase() = 0;
	
	virtual bool readBuffer() = 0;
	virtual bool writeBuffer() = 0;
	virtual bool verifyBuffer() = 0;
	
	virtual PICMemory* memory() = 0;
};

#endif
