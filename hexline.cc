#include "utils.h"
#include "hexline.h"

bool hexline::readData(unsigned long &addr,unsigned char &length,unsigned char *data)
{
	int count,offset,type;
	while (getline(count,offset,type,data)) {
		if (type == 0x00){	// data record
			addr = offset + (_highAddress << 16);
			length = count;
			return true;
		}else if (type == 0x01){ // end record
			return false;
		}else if (type == 0x02){
			;
		}else if (type == 0x03){
			;
		}else if (type == 0x04){ // high address
			_highAddress = data[0] << 8 | data[1];
		}else if (type == 0x05){
			;
		}
	}
	return false;
}

bool hexline::writeData(unsigned long addr,unsigned char count,const unsigned char *data)
{
	if (_highAddress != (addr>>16)) {
		unsigned char w[2];
		_highAddress = addr>>16;
		w[0] = _highAddress >> 8;
		w[1] = _highAddress & 0xff;
		putline(2,0x0000,0x04,w); // liner high address record
	}
	putline(count,addr&0xffff,0x00,data);
}

bool hexline::getline(int &count,int &offset,int &type,unsigned char *data)
{
	char buf[1024];
	while (fgets(buf, sizeof(buf), _fileHandle)){
		_lineCount++;
		if (buf[0]==':'){
			char *p = buf + 1;
			int dat;
			unsigned char sum;
			
			if (!gethexbyte(&count, p, 1))
				goto error;
			p+=2;
			
			if (!gethexbyte(&offset, p, 2))
				goto error;
			p+=4;
			
			if (!gethexbyte(&type, p, 1))
				goto error;
			p+=2;
			
			sum = count + type + (offset >> 8) + (offset & 0xff);
			
			for (int i=0;i<count;i++){
				if (!gethexbyte(&dat, p, 1))
					goto error;
				p+=2;
				*data++ = (unsigned char)dat;
				sum += dat;
			}
			
			if (!gethexbyte(&dat, p, 1))
				goto error;
			
			sum += dat;
			if (sum)
				goto error;
			
			return true;
		}
	}
	return false;
	
 error:
	fprintf(stderr, "[ERROR] Wrong file format. line %d\n", _lineCount);
	_error = true;
	return false;
}

bool hexline::putline(int count,int offset,int type,const unsigned char *data)
{
	unsigned char sum;
	
	sum = count + (offset >> 8) + (offset & 0xff) + type;
	
	fprintf(_fileHandle, ":%02X%04X%02X", count, offset, type);
	
	for (int i=0; i<count; i++){
		fprintf(_fileHandle, "%02X", data[i]);
		sum += data[i];
	}
	
	fprintf(_fileHandle, "%02X\n", (unsigned char)-sum);
	
	return true;
}
