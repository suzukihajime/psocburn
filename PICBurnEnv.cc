// PICBurnEnv.cc
//    2007.6 Y.Senta
//    2008.4.24 arms22

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include "config.h"
#include "PICBurnEnv.h"
#include "PICDevice.h"
#include "ftd2xx/d2xxProgrammerFactory.h"
#include "utils.h"

void PICBurnCmd::DBDump()
{
	dbprintf(3, "Input Commdand:");
	if (m_bCheck)
		dbprintf(3, " Check");
	if (m_bErase)
		dbprintf(3, " Erase");
	if (m_bWrite)
		dbprintf(3, " Write");
	if (m_bVerify)
		dbprintf(3, " Verify");
	if (m_bRead)
		dbprintf(3, " Read");
	
	dbprintf(3, "\n");
	if (m_bLVP)
		dbprintf(3, "Low Voltage mode\n");
	if (m_bQuite)
		dbprintf(3, "sirent mode\n");
	
	if (m_pBPS)
		dbprintf(3, "BPS [%s]\n", m_pBPS);
	if (m_pDeviceName)
		dbprintf(3, "Device [%s]\n", m_pDeviceName);
	if (m_pChipName)
		dbprintf(3, "Chip [%s]\n", m_pChipName);
	if (m_pNewBPS)
		dbprintf(3, "NEW BPS [%s]\n", m_pNewBPS);
	if (m_pInputHEXPath)
		dbprintf(3, "Input File [%s]\n", m_pInputHEXPath);
	if (m_pOutputHEXPath)
		dbprintf(3, "Output File [%s]\n", m_pOutputHEXPath);
	
	if (m_bAdvance)
		dbprintf(3, "Advance Writing mode\n");
}

bool PICBurnCmd::ChipAccessCmd()
{
	if (m_bErase || m_bWrite || m_bVerify || m_bRead)
		return true;
	return false;
}

bool PICBurnCmd::isCommand()
{
	if (m_bCheck || m_bErase || m_bWrite || m_bVerify || m_bRead || m_pNewBPS)
		return true;
	return false;
}

PICProgrammer* PICBurnCmd::CreateProgrammer()
{
	PICProgrammer* programmer=NULL;
	if (m_pProgrammerName){
		if (strncasecmp(m_pProgrammerName, "ftd2xx", sizeof("ftd2xx")) == 0){
			programmer = d2xxProgrammerFactory::create();
		}
	}
	return programmer;
}

//////////////////////////////////////

bool PICBurnEnv::Init(const char *szPath)
{
	FILE *fin;
	bool ret;
	
	fin = fopen(szPath, "r");
	if (!fin){
	  char *p = (char *)strrchr(szPath, '/');
		if (p==NULL){
		  p = (char *)strrchr(szPath, '\\');
		}
		if (p){
			p++;
			fin = fopen(p, "r");
		}
	}
	
	if (!fin){
		fprintf(stderr, " [ERROR]: Can not open define file (%s)\n", szPath);
		return false;
	}
	
	dbprintf(1, "Load %s\n", szPath);
	
	ret = PICDevice::loadDeviceList(fin);
	fclose(fin);
	return ret;
}

void PICBurnEnv::Final()
{
	if (m_pStrategy){
		m_pStrategy->final();
		delete m_pStrategy;
		m_pStrategy = NULL;
	}
	
	if (m_pProgrammer){
		m_pProgrammer->close();
		delete m_pProgrammer;
		m_pProgrammer = NULL;
	}
}

void PICBurnEnv::DumpDevice()
{
	fprintf(stderr, "Supported Device: ");
	PICDevice::dumpAllDevices(stderr);
}

bool PICBurnEnv::DoCmd(PICBurnCmd &cmd)
{
	if (!cmd.isCommand()){
		if (!cmd.m_bQuite)
			fprintf(stderr, "nothing to do\n");
		return true;
	}
	
	PICDevice *device = 0;
	bool autodetect = true;
	
	if (cmd.m_pChipName){
		device=PICDevice::searchDeviceFromName(cmd.m_pChipName);
		if (!device){
			fprintf(stderr, "[ERROR]: [%s] is not supported Chip\n", cmd.m_pChipName);
			return false;
		}
		autodetect = false;
	}
	
	int nNewBpsNo=0;
	if (cmd.m_pNewBPS){
		if (!strcmp(cmd.m_pNewBPS, "9600"))
			nNewBpsNo=0;
		else if (!strcmp(cmd.m_pNewBPS, "19200"))
			nNewBpsNo=1;
		else if (!strcmp(cmd.m_pNewBPS, "38400"))
			nNewBpsNo=2;
		else if (!strcmp(cmd.m_pNewBPS, "57600"))
			nNewBpsNo=3;
		else if (!strcmp(cmd.m_pNewBPS, "115200"))
			nNewBpsNo=4;
		else {
			fprintf(stderr, "[ERROR]: Can not change [%s]bps\n", cmd.m_pBPS);
			return false;
		}
	}
	
// 	if (cmd.m_bRead && !cmd.m_pOutputHEXPath){
// 		fprintf(stderr, "[ERROR]: Read command requires file path\n");
// 		return false;
// 	}
	
	if (cmd.m_bTest){
		;
	}else{
 		if ((cmd.m_bWrite || cmd.m_bVerify) && !cmd.m_pInputHEXPath){
			fprintf(stderr, "[ERROR]: Program/Verify command requires file path\n");
			return false;
		}
	}
	
	FILE *fpi=NULL;
	FILE *fpo=NULL;
	
	if (cmd.m_pInputHEXPath){
		if (strcmp(cmd.m_pInputHEXPath, "-")){
			fpi=fopen(cmd.m_pInputHEXPath, "r");
			if (!fpi){
				fprintf(stderr, "[ERROR]: Can not open [%s]\n", cmd.m_pInputHEXPath);
				return false;
			}
		} else {
			fpi=stdin;
		}
	}
	
	if (cmd.m_pOutputHEXPath){
		if (strcmp(cmd.m_pOutputHEXPath, "-")){
			fpo=fopen(cmd.m_pOutputHEXPath, "w");
			if (!fpo){
				fprintf(stderr, "[ERROR]: Can not open [%s]\n", cmd.m_pOutputHEXPath);
				if (cmd.m_pInputHEXPath)
					fclose(fpi);
				return false;
			}
		} else {
			fpo=stdout;
		}
	}
	
	m_pProgrammer = cmd.CreateProgrammer();
	m_pStrategy = NULL;
	
	do{
		if (!m_pProgrammer){
			fprintf(stderr, "[ERROR]: [%s] is not supported Programmer\n", cmd.m_pProgrammerName);
			break;
		}
		
		if (!m_pProgrammer->open(cmd.m_pDeviceName))
			break;
		
		/*
		if (autodetect){
			device = PICDevice::deviceAutoDetect(m_pProgrammer);
			if (!device){
				fprintf(stderr, "[ERROR]: device not detected\n");
				break;
			}else{
				fprintf(stderr, "[%s] detected\n", device->GetName());
			}
		}
		*/
		
		m_pStrategy = device->createStrategy(m_pProgrammer);
		
		/* expand all of hex file to memory before checking jtag id */
		if (cmd.m_bTest){
			m_pStrategy->memory()->setTestData();
		}else if (cmd.m_bWrite || cmd.m_bVerify){
			if (!m_pStrategy->memory()->readFromFile(fpi))
				break;
		}
		
		if (!autodetect && cmd.m_bCheck){
			if (m_pStrategy->deviceCheck())
				fprintf(stderr, "[%s] connected\n", device->GetName());
			else
				fprintf(stderr, "[%s] not connected\n", device->GetName());
			break;
		}
		
		if (!m_pStrategy->deviceCheck())
			break;
		
		if (cmd.m_bErase){
			if (!m_pStrategy->chipErase())
				break;
		}
		
		if (cmd.m_bRead){
			if (!m_pStrategy->readBuffer())
				break;
			if (!cmd.m_pOutputHEXPath){
				char path[1024];
				strncpy(path, device->GetName(), 1024);
				strncat(path, ".hex", 4);
				cmd.m_pOutputHEXPath=setstring(path);
				fpo=fopen(cmd.m_pOutputHEXPath, "w");
				if (!fpo){
					fprintf(stderr, "[ERROR]: Can not open [%s]\n", cmd.m_pOutputHEXPath);
					break;
				}
			}
			if (!m_pStrategy->memory()->saveToFile(fpo))
				break;
		}
		
		if (cmd.m_bWrite){
			if (!m_pStrategy->writeBuffer())
				break;
		}
		
		if (cmd.m_bVerify){
			if (!m_pStrategy->verifyBuffer())
				break;
		}
	}while(0);
	
	if (m_pStrategy){
		m_pStrategy->final();
		delete m_pStrategy;
		m_pStrategy = NULL;
	}
	
	if (m_pProgrammer){
		m_pProgrammer->close();
		delete m_pProgrammer;
		m_pProgrammer = NULL;
	}
	
	if (fpi)
		fclose(fpi);
	
	if (fpo){
		fclose(fpo);
#ifdef HAVE_CHOWN
		if (strcmp(cmd.m_pOutputHEXPath, "-"))
			chown(cmd.m_pOutputHEXPath, getuid(), getgid());
#endif
	}
	
	return true;
}
