# psocburn -- FT232H-powered PSoC 1/3/5 firmware flasher, forked from 'picburn'

*Important note: the project is archived.*

### Prerequisites
	
* D2XX Direct Driver (http://www.ftdichip.com/Drivers/D2XX.htm)

### Installation

```
./configure
make
make install
```

### Usage 

Flashing CY8C3866

```
psocburn -c CY8C3866 -pv -i something.hex
```

### Supported chips

currently only PSoC3 devices are supported.
CY8C3866 and CY8C3866ES3 were tested.

