// PSoC3Strategy.h
//    2011.12 aasoukai128

#ifndef PSoC3STRATEGY_H
#define PSoC3STRATEGY_H

#include "PICStrategy.h"
#include "PSoC3Memory.h"
#include "PSoC3Command.h"

class PSoC3Strategy : public PICStrategy
{
	PSoC3Memory m_memory;
	PSoC3Command m_piccmd;
	PICProgrammer::PowerSequence m_enterseq;
	PICProgrammer::PowerSequence m_exitseq;
	unsigned long  m_address;
	unsigned short m_osccal[4];
	
public:
	PSoC3Strategy(PICDevice &dev,PICProgrammer &prog);
	~PSoC3Strategy();
	
	bool final();
	bool deviceCheck();
	bool chipErase();
	bool readBuffer();
	bool writeBuffer();
	bool verifyBuffer();
	
	PSoC3Memory* memory() { return &m_memory; }
	
private:
	bool reset(void);
	bool writeMemory(unsigned char *buf, unsigned char *conf, int rowCount, bool isEccEnable, int temp);
	bool readMemory(unsigned char *buf, unsigned char *conf, int rowCount, bool isEccEnable);
	unsigned int readChecksum(void);
	bool verifyMemory(unsigned char *buf, unsigned char *conf, int rowCount, bool isEccEnable);
	unsigned int checkTemperature(void);
	bool writeNVLatch(unsigned char *buf, int size);
};

#endif
