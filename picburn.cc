// picburn.cc
//    2007.6 Y.Senta
//    2008.5 arms22
//    2012.2 aasoukai128

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "PICBurnEnv.h"
#include "utils.h"
#include "config.h"

#define DEF_DEVICE "/dev/ttyS0"
#define DEF_BPS "2000000"
#define DEF_PROGRAMMER "ftd2xx"

#ifndef CHIPPROPATY
#define CHIPPROPATY	"psocburn.dev"
#endif

#ifndef VERSION
#define VERSION "DEBUG"
#endif

int g_nDebugLevel=0;

void help(int n){
	if (n==1)
		fprintf(stderr, "[ERROR]: Invalid option(s)\n\n");
	if (n==3)
		fprintf(stderr, "[ERROR]: No additional information\n\n");
	
	fprintf(stderr, "Usage: psocburn <-k/e/p/r/v> <-i/o/f PATH> <-d PORT> <-c CHIP> <-w PROGRAMMER>\n");
	
	fprintf(stderr, "  -e: Erase\n");
	fprintf(stderr, "  -p: Program data\n");
	fprintf(stderr, "  -r: Read data\n");
	fprintf(stderr, "  -v: Verify data\n");
	fprintf(stderr, "  -c: Chip select\n");
	fprintf(stderr, "  -d: Device setting (Default: %s)\n", DEF_DEVICE);
	fprintf(stderr, "  -i: Input hex file (\"-\" means STDIN)\n");
	fprintf(stderr, "  -o: Output hex file (\"-\" means STDOUT)\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "  -f: device propaty File (Default: %s)\n", CHIPPROPATY);
	fprintf(stderr, "  -k: checK hardware connection\n");
	fprintf(stderr, "  -w: optional hardware (Default: %s)\n", DEF_PROGRAMMER);
// 	fprintf(stderr, "  -q: Quite mode\n");
// 	fprintf(stderr, "\n");
// 	fprintf(stderr, "  -s: communication Speed (Default: 38400)\n");
// 	fprintf(stderr, "  -x: eXchange hardware bps\n");
// 	fprintf(stderr, "  -l: Low voltage programming mode\n");
// 	fprintf(stderr, "  -n: No advance writing mode\n");
	fprintf(stderr, "\n");
	fprintf(stderr, "  -b: deBug lebel\n");
	fprintf(stderr, "  -h: Help");
	if (n==2)
		fprintf (stderr, "... you know this one.");
	fprintf(stderr, "\n");
}

PICBurnEnv g_env;

void handler(int)
{
	g_env.Final();
	fprintf(stderr, " ^C\n");
}

int main(int argc, char **argv)
{
	PICBurnCmd pbcmd;
	
	int nHelpLevel=0;
	char *pDefFile=NULL;
	
	for (int i=1;i<argc;i++){
		char cCmd=0;
		if (argv[i][0]=='-'){
			int j=1;
			while (argv[i][j] && !nHelpLevel){
				switch(argv[i][j]){
					case 't':
						pbcmd.m_bTest=true;
						break;
					case 'k':
						pbcmd.m_bCheck=true;
						break;
					case 'e':
						pbcmd.m_bErase=true;
						break;
					case 'p':
						pbcmd.m_bErase=true;
						pbcmd.m_bWrite=true;
						break;
					case 'v':
						pbcmd.m_bVerify=true;
						break;
					case 'r':
						pbcmd.m_bRead=true;
						break;
					case 'l':
						pbcmd.m_bLVP=true;
						break;
					case 'n':
						pbcmd.m_bAdvance=false;
						break;
					case 'q':
						pbcmd.m_bQuite=true;
						break;
					case 'w':
					case 'd':
					case 'c':
					case 's':
					case 'i':
					case 'o':
					case 'f':
					case 'x':
					case 'b':
						cCmd=argv[i][j];
						break;
					case 'h':
						nHelpLevel=2;
						break;
					default:
						nHelpLevel=1;
						break;
				}
				j++;
				
				if (cCmd){
					if (argv[i][j])
						nHelpLevel=3;
					break;
				}
			}
		} else {
			nHelpLevel=1;
		}
		
		if (cCmd){
			i++;
			if (i==argc){
				nHelpLevel=3;
			} else {
				char *p=setstring(argv[i]);
				switch(cCmd){
					case 'w':
						pbcmd.m_pProgrammerName=p;
						break;
					case 'd':
						pbcmd.m_pDeviceName=p;
						break;
					case 'c':
						pbcmd.m_pChipName=p;
						break;
					case 's':
						pbcmd.m_pBPS=p;
						break;
					case 'i':
						pbcmd.m_pInputHEXPath=p;
						break;
					case 'o':
						pbcmd.m_pOutputHEXPath=p;
						break;
					case 'x':
						pbcmd.m_pNewBPS=p;
						break;
					case 'f':
						pDefFile=p;
						break;
					case 'b':
						g_nDebugLevel=atoi(p);
						delete [] p;
						break;
				}
			}
		}
	}
	
	if (!pbcmd.m_bQuite)
		fprintf(stderr, "psocburn ver " VERSION "\n\n");
	
	if (g_nDebugLevel){
		dbprintf(1, "DebugLevel %d\n", g_nDebugLevel);
		pbcmd.DBDump();
		if (pDefFile)
			dbprintf(1, "ChipDefFile  [%s]\n",pDefFile);
	}
	
	if (!pDefFile)
		pDefFile=setstring(CHIPPROPATY);
	
	if (!pbcmd.m_pDeviceName)
		pbcmd.m_pDeviceName=setstring(DEF_DEVICE);
	
	if (!pbcmd.m_pBPS)
		pbcmd.m_pBPS=setstring(DEF_BPS);
	
	if (!pbcmd.m_pProgrammerName)
		pbcmd.m_pProgrammerName=setstring(DEF_PROGRAMMER);
	
	signal(SIGINT, handler);
	
	if (g_env.Init(pDefFile)){
		if (nHelpLevel){
			help(nHelpLevel);
			g_env.DumpDevice();
		} else {
			g_env.DoCmd(pbcmd);
		}
	}
	g_env.Final();
	
	if (pDefFile)
		delete [] pDefFile;
	
	return 0;
}
