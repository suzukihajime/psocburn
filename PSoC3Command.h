// PSoC3Command.h
//    2011.12 aasoukai128

#ifndef PSoC3COMMAND_H
#define PSoC3COMMAND_H

#include "PICProgrammer.h"

class PSoC3Command {
	PICProgrammer &m_prog;
	int m_increment;
	
public:
	
	PSoC3Command(PICProgrammer &prog);
	~PSoC3Command();
	
	// Primitive PSoC3/5 Command
	bool loadDataForApacc(unsigned int addr,unsigned int data);
	bool pollDataInApacc(unsigned int addr,unsigned int data);
	bool setDMAAccessWidth(int width);
	bool readDataFromApacc(unsigned int addr,unsigned int *rbuf);
	bool readMultipleBytesFromApacc(unsigned int addr,unsigned char *rbuf, int length);
	void readDataFromDpacc(unsigned int *rbuf);
	void beginCommand(void);
	void endCommand(void);
	
	// Read/Write Sequence
	int wordWriteCycle(int cmd,int begin,int end,int wai,const unsigned char *wbuf,int count);
	int wordReadCycle(int cmd,unsigned char *rbuf,int count);
	
	// Enter/Exit Program Verify Mode
	bool enterDebugMode(void);
	void exitDebugMode(void);
	void configureNonVolatileLatch(void);
private:
};

#endif
