// PSoC1Strategy.h
//    2012.2 aasoukai128

#ifndef PSoC1STRATEGY_H
#define PSoC1STRATEGY_H

#include "PICStrategy.h"
#include "PSoC1Memory.h"
#include "PSoC1Command.h"

class PSoC1Strategy : public PICStrategy
{
	PSoC1Memory m_memory;
	PSoC1Command m_piccmd;
	PICProgrammer::PowerSequence m_enterseq;
	PICProgrammer::PowerSequence m_exitseq;
	unsigned long  m_address;
	unsigned short m_osccal[4];
	
public:
	PSoC1Strategy(PICDevice &dev,PICProgrammer &prog);
	~PSoC1Strategy();
	
	bool final();
	bool deviceCheck();
	bool chipErase();
	bool readBuffer();
	bool writeBuffer();
	bool verifyBuffer();
	
	PSoC1Memory* memory() { return &m_memory; }
	
private:
	bool reset(void);
	bool writeMemory(unsigned short *buf,int size);
	bool readMemory(unsigned short *buf,int size);
	bool verifyMemory(unsigned short *buf,int size,unsigned short mask);
};

#endif
