#ifndef PROGRESS_H
#define PROGRESS_H

#include <stdio.h>
#include <time.h>
#include <sys/time.h>

class progress
{
	struct timeval _startTime;
	unsigned _maxValue;
	unsigned _currentValue;
	unsigned _lastValue;
public:
	progress(const char *beginmsg,unsigned max)
		: _maxValue(0) , _currentValue(0) , _lastValue(0) {
		setmax(max);
		begin(beginmsg);
	}
	progress() : _maxValue(0) , _currentValue(0) , _lastValue(0) {
		_startTime.tv_sec = _startTime.tv_usec = 0;
	}
	~progress(){}
	void setmax(unsigned max) {
		_maxValue = max;
		_currentValue = _lastValue = 0;
	}
	void setval(unsigned value) {
		unsigned step = _maxValue / 32;
		if (!step) step = 1;
		_currentValue = value;
		if (_currentValue >= (_lastValue+step)){
			fprintf(stderr,".");
			_lastValue = _currentValue;
		}
	}
	void addval(unsigned value){
		setval(_currentValue+value);
	}
	void begin(const char *beginmsg=NULL) {
		gettimeofday (&_startTime, 0);
		if (beginmsg)
			fprintf(stderr,beginmsg);
	}
	void end(const char *endmsg=NULL) {
		struct timeval endTime;
		double usec;
		gettimeofday (&endTime, 0);
		if (endTime.tv_sec == _startTime.tv_sec){
			usec = endTime.tv_usec - _startTime.tv_usec;
		}else{
			usec = endTime.tv_usec + ((endTime.tv_sec - _startTime.tv_sec - 1) * 1000000.0) + (1000000.0 - _startTime.tv_usec);
		}
		if (endmsg)
			fprintf(stderr,endmsg);	
		fprintf(stderr,"(%.3g sec)\n",usec/1000000.0);
	}
};

#endif
